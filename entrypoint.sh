#!/bin/bash
# entrypoint commands
# setup
# start
# stop
# restart
# kill

# Parse argument
setup=false
setup_local=false
setup_server=false
start_server=false
stop_server=false
   
if [ "$1" = 'setup' ]; then
   # setup
   setup=true
   setup_server=true
elif [ "$1" = 'setup-localhost' ]; then
   # setup on localhost
   setup=true
   setup_local=true
elif [ "$1" = 'start' ]; then
   # start
   start_server=true
   # Verify that the server has been setup previously
   if [ -f /var/opt/webface.config ]; then
      # Source information from config
      source /var/opt/webface.config
   else
      read -p "The server has not been setup previously! Do you wish to set it up now?" -n 1 -r
      #echo    # (optional) move to a new line
      if [[ $REPLY =~ ^[Yy]$ ]]; then
         setup=true
         setup_server=true
      else
         start_server=false
      fi
   fi
elif [ "$1" = 'stop' ]; then
   # stop
   stop_server=true
elif [ "$1" = 'restart' ]; then
# restart - combination of stop and start
   stop_server=true
   start_server=true
elif [ "$1" = 'kill' ]; then
# To kill, the administrator can just kill the docker container, but it might ruin the user databases...
   echo "To force kill the server with data loss, just kill the whole container!\n" \
        "For a carefull shutdown, run the stop command and wait for the processes to finish..."
fi

if [ "$setup" = true ]; then
   # Standard Setup
   
   # Setup SMTP server
   #debconf-show exim4-config # show current configuration
   # Get Full qulifier domain name for the host
   host=$(hostname --fqdn)
   # Get public IP
   ip=$(wget -qO- http://ipecho.net/plain)
   sed -i "s/^exit 101$/exit 0/" /usr/sbin/policy-rc.d
   dpkg-reconfigure exim4-config <<EOF
1
$host
127.0.0.1
$ip; 127.0.0.1; $host; localhost.localdomain; localhost


no
1
no
root
EOF
   
   echo "Reconfiguring MySQL for safety..."
   read -sp "Please provide root password for the MySQL DB: " password
   umask 077
   cat > ~/.my.cnf <<EOF
[mysql]
user="root"
password="$password"
EOF
   
   umask 022 # Default umask
   dpkg-reconfigure mysql-server-5.5 <<EOF
$password
$password
EOF
   
   #/etc/init.d/mysql start
   mysql <<EOF
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
EOF
   
   echo "MySQL Setup Complete!"
   
   echo "Install the WebFace DB!"
   echo "CREATE DATABASE \`webface\`;" | mysql
   # Create webface tables
   mysql -D webface < /usr/opt/www/webface/database/webface.sql
   # Add data from config to webface database
   cat /configs/webface_config.json | python -c "import sys,json;qs=json.loads(sys.stdin.read())['queues'];sys.stdout.write('LOCK TABLES \`config\` WRITE;INSERT INTO \`config\` VALUES ');sys.stdout.write(','.join(\"('%s',%s,%s,%s,%s,%s,%s,%s,'%s','%s')\"%(q['queue'],q['maxqueue'],q['maxactive'],q['maxsource'],q['maxruntime'],q['expiration'],q['quota'],q['timeout'],q['maintainer'],q['state']) for q in qs));sys.stdout.write(';UNLOCK TABLES;')" | mysql -D webface
   protocol=$(jq -r '.protocol' /configs/webface_config.json)
   home=$(jq -r '.home' /configs/webface_config.json)
   host=$(jq -r '.host' /configs/webface_config.json)
   cgi_exe=$(jq -r '.cgi_exe' /configs/webface_config.json)
   manager=$(jq -r '.manager' /configs/webface_config.json)
   tmp_dir=$(jq -r '.tmp_dir' /configs/webface_config.json)
   template_dir=$(jq -r '.template_dir' /configs/webface_config.json)
   log_dir=$(jq -r '.log_dir' /configs/webface_config.json)
   mailbin=$(jq -r '.mailbin' /configs/webface_config.json)
   dfbin=$(jq -r '.dfbin' /configs/webface_config.json)
   refreshtime=$(jq -r '.refreshtime' /configs/webface_config.json)
   waittime=$(jq -r '.waittime' /configs/webface_config.json)
   maxquota=$(jq -r '.maxquota' /configs/webface_config.json)
   expirequota=$(jq -r '.expirequota' /configs/webface_config.json)
   mindiskspace=$(jq -r '.mindiskspace' /configs/webface_config.json)
   maxcommandline=$(jq -r '.maxcommandline' /configs/webface_config.json)
   maxargs=$(jq -r '.maxargs' /configs/webface_config.json)
   servicelog=$(jq -r '.servicelog' /configs/webface_config.json)
   echo "LOCK TABLES \`options\` WRITE;INSERT INTO \`options\` VALUES ('cgiprefix','${protocol}://${host}',NULL),('cgiexec','${cgi_exe}',NULL),('manager','${manager}',NULL),('webfacehome','${home}',NULL),('usrtmp','${tmp_dir}',NULL),('templates','${template_dir}',NULL),('webfacetmp','${tmp_dir}/jobs',NULL),('servertmp','${tmp_dir}/server',NULL),('logdir','${log_dir}',NULL),('mailbin','${mailbin}',NULL),('dfbin','${dfbin}',NULL),('refreshtime','${refreshtime}',NULL),('waittime','${waittime}',NULL),('maxquota','${maxquota}',NULL),('expirequota','${expirequota}',NULL),('mindiskspace','${mindiskspace}',NULL),('maxcommandline','${maxcommandline}',NULL),('maxargs','${maxargs}',NULL),('servicelog','${log_dir}/${servicelog}',NULL);UNLOCK TABLES;" | mysql -D webface
   
   echo "Create WebFace MySQL account!"
   username=$(jq -r '.user' /configs/webface_config.json)
   password=$(jq -r '.password' /configs/webface_config.json)
   
   mysql -D webface -e "CREATE USER '"$username"'@'"$host"' IDENTIFIED BY '"$password"';"
   mysql -D webface -e "GRANT all ON webface.* TO '"$username"' IDENTIFIED BY '"$password"';"
   
   # Fix WebFace permissions
   chown -R www-data:mysql /var/lib/mysql /var/run/mysqld
   
   # Fix MySQL Permissions
   chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
   
   # Setup Apache
   /apache-entrypoint.sh
   cp /configs/apache2.conf /etc/apache2/apache2.conf
   cp /configs/serve-cgi-bin.conf /etc/apache2/conf-available/serve-cgi-bin.conf
   
   # Enable Apache2 Modules
   a2enmod alias mime ssl cgi fcgid
   
   # Create webface directories and logfiles
   mkdir -p $log_dir $tmp_dir/server $tmp_dir/jobs  
   touch $log_dir/service.log
   # Fix WebFace Permissions
   chown -R www-data:www-data $home $log_dir $tmp_dir $tmp_dir/server $tmp_dir/jobs /var/log/webface/service.log
   chmod g+w /var/log/webface/service.log
   
   # Set Server Timezone (specified by PHP_TIMEZONE variable in Dockerfile)
   unlink /etc/localtime && ln -s /usr/share/zoneinfo/Etc/$PHP_TIMEZONE /etc/localtime
   
   # Create own SSL certificates
   # WARNING: these certificates are not trusted by the outside, so they are
   #          only good for testing locally! For production/public servers,get a
   #          certificate through CA!
   echo "Creating Localhost test SSL certificates..."
   cd /etc/ssl/private/
   openssl req -x509 -out localhost.crt -keyout localhost-private.pem \
           -newkey rsa:2048 -nodes -sha256 \
           -subj '/CN=localhost' -extensions EXT -config <( \
              printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
   chown root:root localhost.crt localhost-private.pem
   chmod 644 localhost.crt
   chmod 640 localhost-private.pem
   mv localhost.crt ../certs/
   cd -
   
   touch /var/opt/webface.config
fi

if [ "$setup_server" = true ]; then
   # Production Server Specific Setup
   
   # Copy /var/log to /logs to store for later use
   rsync -a /var/log/* /logs/
elif [ "$setup_local" = true ]; then
   # Localhost Specific setup
   
   # Copy /var/log to /logs to store for later use
   rsync -a /var/log/* /logs/
fi

if [ "$stop_server" = true ]; then
   # stop - stop new jobsubmissions by killing the apache server, wait for jobs to finish, and kill the remaining processes.
   # Verify that the server is running
   # Stop servers
   /etc/init.d/mysql stop
   apache2 stop
   
   echo "The server is stopped!"
fi

if [ "$start_server" = true ]; then
   # start - startup mysql server/apache server/webface server - require passwords if config is not set!
   
   if [ ! "$setup_server" = true ]; then
      # Fix MySQL Permissions
      chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
      # Start MySQL server
      /etc/init.d/mysql start
   fi
   
   # Start SMTP server
   /exim-entrypoint.sh exim -bd -q15m -v &
   
   # Start Apache server
   apachectl start
   
fi

# Execute arguments if any
if [ ! -z "${@:2:2}" ]; then
   exec "${@:2:2}"
fi
