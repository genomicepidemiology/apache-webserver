############################################################
# Dockerfile to build image
############################################################

# Load base Docker image
FROM antage/apache2-php5

RUN groupadd -r mysql && useradd -r -g mysql mysql

# Disable Debian frontend interaction promts
ENV DEBIAN_FRONTEND noninteractive

# Install dependencies with apt-get
RUN set -ex; \
    apt-get update -qq; \
    apt-get upgrade -qq; \
    apt-get install -y --no-install-recommends -qq \
        apt-utils \
        build-essential \
        exim4-daemon-light \
        gcc \
        jq \
        less \
        libapache2-mod-fcgid \
        libz-dev \
        mailutils \
        mysql-server \
        openssl \
        perl \
        php5-curl \
        php5-mysql \
        rsync \
        sudo \
    ; \
    apt-get clean && \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    find /var/log -type f | while read f; do echo -ne '' > $f; done;

# Install perl modules
RUN curl -L https://cpanmin.us -k | perl - App::cpanminus && \
    cpanm BSD::Resource && \
    cpanm CGI::Fast && \
    cpanm CJFIELDS/BioPerl-1.6.924.tar.gz --force && \
    cpanm Data::Dumper && \
    cpanm Data::Structure::Util && \
    cpanm File::Temp && \
    cpanm Getopt::Long && \
    cpanm IPC::PerlSSH && \
    cpanm JSON && \
    cpanm JSON::Parse && \
    cpanm Mail::Sender && \
    cpanm Try::Tiny::Retry; \
    rm -rf $HOME/.cpan/build/*            \
           $HOME/.cpan/sources/authors/id \
           $HOME/.cpan/cpan_sqlite_log.*  \
           /tmp/cpan_install_*.txt

# Re-enable Debian frontend interaction promts
ENV DEBIAN_FRONTEND Teletype

# Set environment
#ENV CREATE_USER_UID 1000
#ENV CREATE_USER_GID 1000
#ENV CREATE_SYMLINKS (for example: "/var/www/dir1>/var/dir1,/var/www/dir2>/var/dir2")
#ENV APACHE_COREDUMP
# USER/GRP ID of www-data: 33:33
ENV APACHE_RUN_USER 'www-data'
ENV APACHE_RUN_GROUP 'www-data'
#ENV APACHE_MODS 'alias,mime,cgi,fcgid'
# APACHE_MODS variable does not seem to work properly, some mods are not enabled. Using a2enmod instead
ENV APACHE_DOCUMENT_ROOT '/var/www/htdocs'
#(/var/www/html by default)
ENV APACHE_SERVER_NAME 'localhost'
#(hostname by default)
#ENV APACHE_ALLOW_OVERRIDE ('None' by default)
#ENV APACHE_ALLOW_ENCODED_SLASHES ('Off' by default)
#ENV APACHE_MAX_REQUEST_WORKERS (32 by default)
#ENV APACHE_MAX_CONNECTIONS_PER_CHILD (1024 by default)

# SSL Settings
ENV SSLCertificateFile 'localhost.crt'
ENV SSLCertificateKeyFile 'localhost-private.pem'
# NOTE: Remeber to mount the directory with the certificates above to /etc/ssl/private/servercerts/

# PHP settings
ENV PHP_MODS 'curl,date,ereg,hash,json,libxml,mhash,mysql,mysqli,mysqlnd,openssl,session,sqlite3,xml,xmlreader,xmlwriter,zip,zlib'
# ,pdo_mysql,pdo_sqlite
ENV PHP_TIMEZONE 'GMT-2'
#('UTC' by default)
ENV PHP_SMTP 'mail.cge.dtu.dk'
#  - MTA SMTP IP-address/hostname
ENV PHP_SMTP_FROM 'webserver@cge.dtu.dk'
# - default From header for mail (example: 'noreply@example.org')
#ENV PHP_MBSTRING_FUNC_OVERLOAD - mbstring.func_overload (0 by default).
#ENV PHP_ALWAYS_POPULATE_RAW_POST_DATA - always_populate_raw_post_data (0 by default).
#ENV PHP_NEWRELIC_LICENSE_KEY - Newrelic agent license key (empty and disabled by default).
#ENV PHP_NEWRELIC_APPNAME - Newrelic application name (empty by default).

# Install WebFace
RUN mkdir /var/www/cgi-bin/ /var/www/fcgi-bin/
COPY Webface2.5/htdocs /var/www/htdocs
COPY configs /configs
COPY entrypoint.sh /
WORKDIR /usr/opt/www/webface
COPY Webface2.5/templates templates
COPY Webface2.5/perl-lib perl-lib
COPY Webface2.5/bin bin
COPY Webface2.5/fcgi-bin/* /var/www/fcgi-bin/
COPY Webface2.5/database database
COPY cgi-bin/* /var/www/cgi-bin/

# Add exim SMTP files
COPY exim-entrypoint.sh /
COPY set-exim4-update-conf /bin/set-exim4-update-conf

# Set execute permissions
RUN mv /docker-entrypoint.sh /apache-entrypoint.sh; \
    chmod a+x /entrypoint.sh; \
    chmod a+x /exim-entrypoint.sh; \
    chmod a+x /apache-entrypoint.sh; \
    chmod a+x /bin/set-exim4-update-conf; \
    chmod +x /var/www/fcgi-bin/* /var/www/cgi-bin/*;

## Start server
ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]

# Setup .bashrc file for convenience during debugging
RUN echo "alias ls='ls -h --color=tty'\n"\
"alias ll='ls -lrt'\n"\
"alias l='less'\n"\
"alias du='du -hP --max-depth=1'\n"\
"alias cwd='readlink -f .'\n"\
"PATH=$PATH\n">> ~/.bashrc
