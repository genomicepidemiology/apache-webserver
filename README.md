# apache-webserver #

This repository contains the scripts and files necessary to setup the Apache/PHP
web server needed to run the services for the Center for Genomic Epidemiology
(CGE).

There are several parts to this:

*  Apache2 web server with PHP5 support
*  MySQL server
*  SMTP server
*  WebFace CGI server
*  HTML5/JavaScript/AngularJS file upload application (NOT IMPLEMENTED YET)
*  CGE services web interfaces (covers a plural of bioinformatic services) (NOT IMPLEMENTED YET)
*  PHP service infrastructure for the CGE services (NOT IMPLEMENTED YET)
*  CGE platform (User login system + batch upload + bacterial analysis pipeline) (NOT IMPLEMENTED YET)
*  CGI scripts (SVG-to-image converter) (NOT IMPLEMENTED YET)
*  Python backend service infrastructure (NOT IMPLEMENTED YET)

This web server will need parallel docker containers install where the actual
services are running, and it needs the Resource manager TORQUE/MOAB installed
on the main machine to handle the backend job queue.

Authors: 
   Martin Christen Frølund Thomsen,

Installation Via Docker Hub
===========================
```bash
docker pull genomicepidemiology/apache-webserver
docker tag genomicepidemiology/apache-webserver cge-webserver
```

Installation Via Docker
=======================
```bash
# Create Docker tmpfs volume for PHP sessions
docker volume create --driver local \
    --opt type=tmpfs \
    --opt device=tmpfs \
    --opt o=size=100m,uid=1000 \
    sessions

# Download webserver
git clone https://bitbucket.org/genomicepidemiology/apache-webserver apache-webserver
cd apache-webserver
```

Setup and save configured server
================================
```bash
# Build the server
docker-compose -f configure-test.yml build

# Configure the server (Interactive - e.i. MySQL DB password needed!)
docker-compose -f configure-test.yml run cge-webserver setup-localhost

# Save the configurated container as an image
docker commit $(docker ps --latest --quiet) cge-webserver:configured

# Start the server
docker-compose -f test.yml up -d

# Connect to server
docker exec -it webserver bash

# Shutdown server
docker-compose -f test.yml down

# Clean Docker
docker container prune -f
docker image prune -f
docker volume prune -f
docker network prune -f
```

Internal Interactions
=====================
```bash
# Track Apache2 error log
tail -f /var/log/apache2/error.log 

# Restart the Apache web server
apachectl restart

# Test Mail
echo "Test Message" | mail -r reply-to@mail.com -s "Test Subject" receiver@mail.com

# OBS: Gmail etc. automatically deletes these types of mails, so look in your trash!

# Detach from the container without stopping it
tail -f /dev/null
# CTRL+P CTRL+Q
```

External Tests
==============
While testing the server on the localhost, you might run into the issue that
your browser will not allow you to connect, since the certificate is not signed
by a trusted certificate authority. To get around this issue, you can add a
security exception for the localhost. After that, the browser should allow you
to connect through HTTPS.
For a production server, you should provide the certificate signed by CA for
your domain to the server so that this issue does not happen there.

## Test the dummy shell CGI script in a browser ##
https://localhost/cgi-bin/hello.sh

## Test the dummy python CGI script in a browser ##
https://localhost/cgi-bin/hello.py

## Test the WebFace interface in a browser ##
https://localhost/services/myserver/index.html

Submit a job and wait for it to finish successfully, to ensure the server run
as it should.

Usage - Running the Server (TODO!)
==========================
## Create data volume ##
```bash

docker volume create --name cge-data

docker volume create --driver local \
    --opt type=tmpfs \
    --opt device=tmpfs \
    --opt o=size=100m,uid=1000 \
    foo
```

## Get the server online ##
```bash
#!bash
docker-compose up
```

## Notes ##
*  sessions: (tmpfs is recommended)
*  apache2-coredumps: (optional)


## Exposed ports ##
*  8080/tcp


## Important Unix Users ##
*  root - root account, the one everything is installed from, and is running the server
*  www-data - Apache user, also the user used to run WebFace
*  mysql - The MySQL is used to set up the whole MySQL server

Troubleshoot
============

## Are you stuck on one of the following messages? ##
*  Waiting for SSH to be available...
*  Warning: failed to get default registry endpoint from daemon...
*  Error checking TLS connection: Host is not running
*  Cannot connect to the Docker daemon...

Then try unsetting your docker environment:
´´´bash
eval $(docker-machine env --unset)
´´´
If that does not do it, try closing down all docker related processes, and
restart docker.

License
=======
This repository contains software from external sources, that is licensed
independently from the rest.

## WebFace ##
Copyright 2014 Hans-Henrik Stærfeldt, Technical University of Denmark

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Everything Else ##
Copyright 2016 Center for Genomic Epidemiology, Technical University of Denmark

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
