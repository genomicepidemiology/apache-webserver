WebFace
=======
Website Interface for handling CGI form submissions to run server side services.

Author: Hans-Henrik Stærfeldt
Modifier: Martin Christen Frølund Thomsen

## Webface Service Interface ##
WebFace provides a PHP template for setting up a HTML form for a given service.
Each service is associated with a config file, where the HTTP form data input
can be translated to a command line script call.

## WebFace Fast Common Gateway Interface (Fast CGI) ##
WebFace translates the HTTP forms data to a commandline call via a Fast CGI
script written in Perl, and a library of Perl modules. The stdout of the
commandline call is written to a HTML template and displayed to the user.

## Features ##
WebFace offers several features:
* Job restriction, job handling and job queueing
* User-based job submission limitation

License
=======
Copyright 2014 Hans-Henrik Stærfeldt, Technical University of Denmark

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
