-- MySQL dump 10.13  Distrib 5.1.46, for suse-linux-gnu (x86_64)
--
-- Host: localhost    Database: webface
-- ------------------------------------------------------
-- Server version	5.1.46-log

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `queue` varchar(128) NOT NULL,
  `maxqueue` bigint(20) NOT NULL,
  `maxactive` bigint(20) NOT NULL,
  `maxsource` bigint(20) NOT NULL,
  `maxruntime` bigint(20) NOT NULL,
  `expiration` bigint(20) NOT NULL,
  `quota` bigint(20) NOT NULL,
  `timeout` bigint(20) DEFAULT NULL,
  `maintainer` varchar(128) NOT NULL,
  `state` enum('scheduling','stopped','maintenance','disabled') DEFAULT NULL,
  PRIMARY KEY (`queue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `name` varchar(128) NOT NULL,
  `value` text,
  `comment` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
CREATE TABLE `queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobid` varchar(32) NOT NULL,
  `queue` varchar(128) NOT NULL,
  `service` varchar(128) NOT NULL,
  `source` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `status` enum('active','queued','finished','failed','rejected','killed','crashed','sanitized','banned','disabled') DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `runtime` bigint(20) DEFAULT NULL,
  `rusage` longtext DEFAULT NULL,
  `stamp` bigint(20) DEFAULT NULL,
  `expire` datetime DEFAULT NULL,
  `delivered` enum('yes','no') DEFAULT 'no' NOT NULL,
  `geo` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobid` (`jobid`),
  KEY `queue` (`queue`,`status`,`id`),
  KEY `queue_2` (`queue`,`service`),
  KEY `expire` (`expire`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

--
-- Table structure for table `quota`
--

DROP TABLE IF EXISTS `quota`;
CREATE TABLE `quota` (
  `source` varchar(64) NOT NULL,
  `limit` bigint(20) DEFAULT NULL,
  `expire` bigint(20) DEFAULT NULL,
  `stamp` timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`source`),
  KEY `expire` (`expire`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
