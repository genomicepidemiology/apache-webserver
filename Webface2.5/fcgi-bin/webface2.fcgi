#!/usr/bin/env perl
use strict;
use warnings;
use CGI::Fast qw(:standard);
use POSIX;

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{"lib_dir"};
}

use lib "$lib_dir/";
use Webface;
$| = 1;

# Connect to Database
my $conn = sprintf("DBI:mysql:%s:%s:%s", $config->{db}, $config->{host}, $config->{port});
my @database = ($conn, $config->{user}, $config->{password});

#
# PIPE occurs when trying to write/print to a file descriptor that has been
#   closed.  Under mod_fastcgi, if a client aborts a connection before it
#   completes mod_fastcgi does too.  At a minimum, SIGPIPE should be ignored
#   (this is already setup in applications spawned by mod_fastcgi).
#   Ideally, it should result in an early abort of the request handling
#   within your application and a return to the top of the FastCGI accept() loop.
#
# USR1 may be received by FastCGI applications spawned by mod_fastcgi.
#   Apache uses this signal to request a "graceful" process shutdown (e.g. its
#   used by "apachectl restart").  When mod_fastcgi's Process Manager receives
#   USR1 it sends TERM to all of the FastCGI applications it spawned and then
#   exits (its gets restarted by Apache).  This means that, under
#   Apache/mod_fastcgi, a FastCGI application that receives USR1 from Apache
#   will also receive TERM from the process manager.
#   FastCGI applications expected to be run under the control of
#   mod_fastcgi/Apache should handle USR1 by finishing any request in process
#   and then shutting down.
#
# TERM is the standard signal sent to applications to request shutdown.
#   Typically, this is considered a request for "clean" shutdown, i.e. finish
#   anything your in the middle of (within reason), free resources such as
#   database connections, and exit.  When Apache receives TERM, it does not
#   finish handling requests in progress.  Whether or not you finish any
#   request in process when TERM is received is your decision, but keep in
#   mind that under mod_fastcgi/Apache the TERM may be a "graceful" request
#   in disguise.  I always finish any request in process before exiting.
#

my $webface=new Webface(@database);

# Set up signal handlers
sub sig_handler
{
	$webface->requestexit('interrupt');
	#exit(0) if (!defined $webface->{request});
}

$SIG{USR1} = \&sig_handler;
$SIG{TERM} = \&sig_handler;
$SIG{PIPE} = 'IGNORE';
$SIG{CHLD} = 'IGNORE';

# run the loop
$webface->requestloop();
exit 0;
#EOF
