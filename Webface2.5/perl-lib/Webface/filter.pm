package Webface::filter;
use strict;
use Data::Dumper;
use Webface::errorlog;

# construct
sub new
{
	my $self  = {};
	my $class = shift;
	$self = bless($self, $class);
	my $filter = shift;
	$self->set($filter);

	#$self->{debug} = 1;
	return $self;
}

# initialize filter parameters
sub set
{
	my $self   = shift;
	my $filter = shift;
	$filter = "" unless (defined $filter);

	#errorlog("Webface::filter($filter)\n");
	$filter =~ s/^(.*);$/$1/; # Often the filter has an extra ;
	my %filter = split(/;/, $filter);
	my %default = (
		legalhdr => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 -,.',
		xhdr     => '_',
		xchar    => 'x',
		repl     => '',
		skip     => '1234567890 ;.:',
		legal    => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
		maxseq   => undef,
		maxres   => undef,
		maxsize  => undef,
		maxcol   => 80,
		minres   => undef
	);
	for my $i (0 .. 255) { $self->{map}->[$i] = 'none'; }
	my %opt = %filter;
	foreach my $key (keys %default) {

		# apply defaults
		$filter{$key} = $default{$key} unless (exists $filter{$key});
	}
	$self->{colno}        = 1;
	$self->{rlastnewline} = 1;
	$self->{wlastnewline} = 1;
	$self->{seqcnt}       = 0;
	$self->{rescnt}       = 0;
	$self->{truncated}    = 0;
	$self->{state}        = 'skipuntilentry';

	# register legal alphabet
	foreach my $key (keys %filter) {
		next if ($key eq '');
		throw WebfaceConfigError -text => "Unsupported filter key: $key" unless (exists $default{$key});
		$self->{$key} = $filter{$key};
		if (   $key eq 'repl'
			|| $key eq 'skip'
			|| $key eq 'legal')
		{
			$opt{$key} = 1;
			foreach my $c (split(//, $filter{$key})) {
				$self->{map}->[ord($c)] = $key;
			}
		}
	}

	# deduce missing information
	if (not exists $opt{legal}) {
		for my $i (0 .. 255) {
			$self->{map}->[$i] = 'legal' if ($self->{map}->[$i] eq 'none');
		}
	}
	if (not exists $opt{skip}) {
		for my $i (0 .. 255) {
			$self->{map}->[$i] = 'skip' if ($self->{map}->[$i] eq 'none');
		}
	}
	if (not exists $opt{repl}) {
		for my $i (0 .. 255) {
			$self->{map}->[$i] = 'repl' if ($self->{map}->[$i] eq 'none');
		}
	}
	for my $i (0 .. 255) {
		$self->{map}->[$i] = 'skip' if ($self->{map}->[$i] eq 'none');
	}

	# always skip ; and whitespace
	$self->{map}->[ord(";")]  = 'skip';
	$self->{map}->[ord(" ")]  = 'skip';
	$self->{map}->[ord("\t")] = 'skip';
	$self->{map}->[ord("\n")] = 'skip';
	$self->{map}->[ord("\r")] = 'skip';
}

# change state
sub state
{
	my $self  = shift;
	my $state = shift;

	# change state
	warn("$self->{state} -> $state") if ($self->{debug});
	$self->{state} = $state;
}

# map sequence characters
sub map
{
	my $self = shift;
	my $res  = shift;
	my $k    = $self->{map}->[ord($res)];

	# map the residues
	return $k if ($k eq 'skip');
	if ($k eq 'repl') {
		if ($res =~ m/[[:lower:]]/) {
			return lc($self->{xchar});
		} elsif ($res =~ m/[[:upper:]]/) {
			return uc($self->{xchar});
		} else {
			return $self->{xchar};
		}
	}
	return $res;
}

# state: 'skiprest'
sub filterskiprest
{
	my $self = shift;
	my $c    = shift;

	# skip rest of stream
	warn("state: skiprest") if ($self->{debug});

	# truncated if we encounter anything
	if ($c eq '>' && $self->{rlastnewline}) {
		$self->{truncated} = 1;
	}
	$c = $self->map($c);
	if ($c ne 'skip') {
		$self->{truncated} = 1;
	}
	return 1;
}

# state 'skipsequence'
sub filterskipsequence
{
	my $self = shift;
	my $c    = shift;

	# skip rest of current sequence
	warn("state: skipseq") if ($self->{debug});
	if ($c eq '>' && $self->{rlastnewline}) {

		# new sequence start here
		$self->state('header');

		# check minimal sequence length of previous sequence
		if (    $self->{seqcnt}
			and defined $self->{rescnt}
			and defined $self->{minres}
			and $self->{rescnt} < $self->{minres})
		{
			$self->{error}->{minres} = $self->{minres};
		}
		$self->{seqcnt}++;
		$self->{rescnt} = 0;

		# check limits
		return 1 if ($self->checklimits());

		# start header
		$self->print("\n") unless ($self->{wlastnewline});
		$self->print($c);
		return 1;
	} else {

		# encountered a residue
		$c = $self->map($c);
		if ($c ne 'skip') {

			# hit limit for single sequence
			$self->{truncated} = 1;
		}
		return 1;
	}
}

# state 'skipuntilentry'
sub filterskipuntilentry
{
	my $self = shift;
	my $c    = shift;

	# skip until (first) entry start
	warn("state: skipuntilentry") if ($self->{debug});

	# preliminary state.. wait for an entry to start
	if ($c eq '>' && $self->{rlastnewline}) {
		$self->state('header');

		# check minimal sequence length of previous sequence
		if (    $self->{seqcnt}
			and defined $self->{rescnt}
			and defined $self->{minres}
			and $self->{rescnt} < $self->{minres})
		{
			$self->{error}->{minres} = $self->{minres};
		}
		$self->{seqcnt}++;
		$self->{rescnt} = 0;

		# checklimits
		return 1 if ($self->checklimits());

		# start header
		$self->print("\n") unless ($self->{wlastnewline});
		$self->print($c);
		return 0;
	} else {
		if ($c ne "\n") {
			$c = $self->map($c);
			if ($c ne 'skip') {

				# check minimal sequence length of previous sequence
				if (    $self->{seqcnt}
					and defined $self->{rescnt}
					and defined $self->{minres}
					and $self->{rescnt} < $self->{minres})
				{
					$self->{error}->{minres} = $self->{minres};
				}

				# we get sequence, but has no header
				$self->{seqcnt}++;
				$self->{rescnt} = 0;

				# check limits
				return 1 if ($self->checklimits());

				# write header
				$self->print(">Sequence\n");
				$self->state('sequence');
				$self->{colno} = 1;

				# legal or replaced, show it
				$self->print($c);
				$self->{rescnt}++;
				$self->{sizecnt}++;
			}
		}

		# else, we have some junk we're skipping
	}
	return 0;
}

# state 'header'
sub filterheader
{
	my $self = shift;
	my $c    = shift;

	# filter the header
	warn("state: header") if ($self->{debug});
	if ($c eq "\n") {
		$self->state('sequence');
		$self->print($c) unless ($self->{wlastnewline});
		return 0;
	}

	# todo: this wont work right unless string is 'nice'
	#p: don't really know what it means
	$c = $self->{xhdr} unless ($c =~ m/[$self->{legalhdr}]/);
	$self->print($c);
	return 0;
}

# state 'sequence'
sub filtersequence
{
	my $self = shift;
	my $c    = shift;
	warn("state: sequence") if ($self->{debug});

	# we start a new record
	if ($c eq '>' && $self->{rlastnewline}) {

		# check minimal sequence length of previous sequence
		if (    $self->{seqcnt}
			and defined $self->{rescnt}
			and defined $self->{minres}
			and $self->{rescnt} < $self->{minres})
		{
			$self->{error}->{minres} = $self->{minres};
		}
		$self->{rescnt} = 0;
		$self->{seqcnt}++;

		# check limits
		return 1 if ($self->checklimits());

		# start header
		$self->state('header');
		$self->print("\n") unless ($self->{wlastnewline});
		$self->print($c);
		return 0;
	}
	my $oc = $c;
	$c = $self->map($c);
	warn("map $oc->$c") if ($self->{debug});
	if ($c ne 'skip') {

		# legal or replaced, show it
		$self->print("\n")
		  if (($self->{colno} > $self->{maxcol}) && (!$self->{wlastnewline}));
		$self->print($c);
		$self->{rescnt}++;
		$self->{sizecnt}++;

		# check limits
		return 1 if ($self->checklimits());
	} else {
		warn("skipping $oc") if ($self->{debug});
	}
	return 0;
}

# Check limits of a fasta stream
sub checklimits
{
	my $self = shift;

	# check if any limites has been exceeded
	if (defined $self->{maxsize} && $self->{maxsize} < $self->{sizecnt}) {
		$self->{error}->{maxsize} = $self->{maxsize};
		$self->state('skiprest');
		return 1;
	}
	if (defined $self->{maxres} && $self->{maxres} < $self->{rescnt}) {
		$self->{error}->{maxres} = $self->{maxres};
		$self->state('skipsequence');
		return 1;
	}
	if (defined $self->{maxseq} && $self->{maxseq}+1 < $self->{seqcnt}) {
		$self->{error}->{maxseq} = $self->{maxseq};
		$self->state('skiprest');
		return 1;
	}
	return 0;
}

# Add a string of caharcters to the fasta filter
sub addstr
{
	my $self = shift;
	my $str  = shift;

	# if anyrthing is there
	return unless (defined $str);

	# add stuff one char at a time
	if (length($str) > 1) {
		foreach my $c (split(//, $str)) {
			$self->addstr($c);
		}
		return;
	}

	# adding a character
	my $c = $str;
	my $r = 0;
	$c = "\n" if ($c eq "\r");
	warn("addstr($c:" . ord($c) . ":$self->{state})") if ($self->{debug});

	# skip multiple newlines
	return 0 if ($self->{rlastnewline} && $c eq "\n");

	# handle state machine
	if ($self->{state} eq 'skiprest') {
		$r = $self->filterskiprest($c);
	} elsif ($self->{state} eq 'skipuntilentry') {
		$r = $self->filterskipuntilentry($c);
	} elsif ($self->{state} eq 'header') {
		$r = $self->filterheader($c);
	} elsif ($self->{state} eq 'skipsequence') {
		$r = $self->filterskipsequence($c);
	} elsif ($self->{state} eq 'sequence') {
		$r = $self->filtersequence($c);
	} else {
		throw WebfaceSystemError -text => "Unknown fasta filter state $self->{state}";
	}
	$self->{rlastnewline} = ($c eq "\n");
	return $r;
}

# open output file
sub open
{
	my $self = shift;
	my $file = shift;
	if ($file eq '-') {
		$self->{out} = \*STDOUT;
	} else {
		open(FH, ">", $file)
		  or throw WebfaceSystemError -text => "Unable to open data file $file for output";
		$self->{out} = \*FH;
	}
}

# close output file
sub close
{
	my $self = shift;
	$self->print("\n") unless ($self->{wlastnewline});
	return if ($self->{out} == \*STDOUT);
	close($self->{out}) or throw WebfaceSystemError -text => "Could not close file";
	delete $self->{out};

	# check minimal sequence length of last sequence
	if ($self->{seqcnt} and defined $self->{rescnt} and defined $self->{minres} and $self->{rescnt} < $self->{minres}) {
		$self->{error}->{minres} = $self->{minres};
	}
}

# print to output file
sub print
{
	my $self = shift;
	my $str  = shift;

	# assumes either one character at a time or newline at end
	$self->{colno} += length($str);
	print { $self->{out} } $str;
	$self->{wlastnewline} = ($str =~ m/.*\n$/);
	$self->{colno} = 1 if ($self->{wlastnewline});
	warn("colno: $self->{colno}") if ($self->{debug});
}

# Filter a fasta sequence from an open filehandle
sub fastafile
{
	my $self = shift;
	my $ifh  = shift;
	my $file = shift;
	$self->open($file);
	while (<$ifh>) {
		$self->addstr($_);
	}
	$self->close();
}

# Filter a fasta sequence from (optional) ident and sequence
sub fastapaste
{
	my $self = shift;
	my $id   = shift;
	my $seq  = shift;
	my $file = shift;
	$self->open($file);
	$self->addstr(">$id\n") if (defined $id);
	$self->addstr($seq);
	$self->close();
}

# Copy a file line by line
sub copyfile
{
	my $self = shift;
	my $ifh  = shift;
	my $file = shift;
	$self->open($file);
	while (<$ifh>) {
		$self->print($_);
	}
	$self->close();
}

# Copy a text to a file
sub copytext
{
	my $self = shift;
	my $txt  = shift;
	my $file = shift;
	$self->open($file);
	$self->print($txt);
	$self->close();
}
1;
