package Webface::queue;

#use lib '../';    # hack
use strict;
use Error qw(:try);
use DBI 1.614;
use Webface::job;
use Webface::test;
use Webface::errorlog;
use Data::Dumper;

my $sql = {
	"sth.config"       => "SELECT SQL_CACHE * FROM config WHERE queue LIKE ?",
	"quota.list"       => "SELECT * FROM quota WHERE `limit` is NULL or `limit`=0 ORDER BY `limit`,source",
	"quota.get"        => "SELECT * FROM quota WHERE source=?",
	"quota.ban"        => "REPLACE INTO quota VALUES ( ?, 0, NULL )",
	"quota.banupd"     => "UPDATE quota SET `limit`=0 WHERE SOURCE like ?",
	"quota.banmv"      => "UPDATE queue SET `status`='banned' WHERE source LIKE ?",
	"quota.unban"      => "DELETE FROM quota WHERE `limit`=0 AND source LIKE ?",
	"quota.expire"     => "DELETE FROM quota WHERE expire<UNIX_TIMESTAMP() AND expire IS NOT NULL",
	"quota.wipe"       => "DELETE FROM quota WHERE expire IS NULL",
	"quota.wipesource" => "DELETE FROM quota WHERE source LIKE ?",
	"quota.insert"     => "REPLACE INTO quota (`source`,`limit`,`expire`) VALUES (?,?,?+UNIX_TIMESTAMP())",
	"quota.push"       => "REPLACE INTO quota (`source`,`limit`,`expire`) VALUES (?,?,NULL)",
	"quota.delete"     => "DELETE FROM quota WHERE source=?",
	"queue.listqueue"  =>
	  "SELECT SQL_CACHE *,IF(runtime IS NULL AND status='active',UNIX_TIMESTAMP()-stamp,runtime) AS runtime,(expire<=NOW()) AS isexpired FROM queue WHERE ( queue LIKE ? ) AND ( UNIX_TIMESTAMP()-stamp<? OR status='active' OR status='queued' ) ORDER BY queue,status,stamp ASC",
	"queue.listqueuesource" =>
	  "SELECT SQL_CACHE *,IF(runtime IS NULL AND status='active',UNIX_TIMESTAMP()-stamp,runtime) AS runtime,(expire<=NOW()) AS isexpired FROM queue WHERE ( queue LIKE ? AND source LIKE ? ) AND ( UNIX_TIMESTAMP()-stamp<? OR status='active' OR status='queued' ) ORDER BY queue,status,stamp ASC",
	"queue.insert" =>
	  "INSERT INTO queue (jobid,queue,service,source,amount,pid,status,stamp,expire) VALUES (?,?,?,?,1,?,?,UNIX_TIMESTAMP(),DATE_ADD(NOW(),INTERVAL ? SECOND))",
	"queue.updateexpire" => "UPDATE queue SET expire=DATE_ADD(NOW(),INTERVAL ? SECOND) WHERE jobid=?",
	"queue.updatestatus" => "UPDATE queue SET status=?,pid=?,stamp=UNIX_TIMESTAMP() WHERE jobid=? AND status LIKE ?",
	"queue.updatepid"    => "UPDATE queue SET pid=?,stamp=UNIX_TIMESTAMP() WHERE jobid=?",
	"queue.updateemail"  => "UPDATE queue SET email=? WHERE jobid=?",
	"queue.updatejob"    =>
	  "UPDATE queue SET status=?,runtime=?,rusage=?,expire=DATE_ADD(NOW(),INTERVAL ? SECOND),amount=?,stamp=UNIX_TIMESTAMP() WHERE jobid=? AND status LIKE ?",
	"queue.get" =>
	  "SELECT SQL_CACHE *,IF(runtime IS NULL AND status='active',UNIX_TIMESTAMP()-stamp,runtime) AS runtime FROM queue WHERE jobid=?",
	"queue.queryconsumption" =>
	  "SELECT SUM(amount) AS amount FROM queue WHERE source like ? AND expire IS NOT NULL AND expire>=NOW()",
	"queue.queryactive" =>
	  "SELECT queue,source,COUNT(*) AS amount FROM queue WHERE source like ? AND status='active' GROUP BY queue,source",
	"queue.expire" =>
	  "DELETE FROM queue WHERE expire<=NOW() AND expire IS NOT NULL AND status NOT IN('queued','active')",
	"queue.status" =>
	  "SELECT queue,status,count(*) AS cnt,MAX(UNIX_TIMESTAMP()-stamp) AS maxtime, AVG(UNIX_TIMESTAMP()-stamp) AS avgtime FROM queue WHERE status IN ('active','queued') GROUP BY queue,status",
	"queue.globalstatus" =>
	  "SELECT status,count(*) AS cnt,MAX(UNIX_TIMESTAMP()-stamp) AS maxtime, AVG(UNIX_TIMESTAMP()-stamp) AS avgtime FROM queue WHERE status IN ('active','queued') GROUP BY status",
	"queue.getprocessrate" =>
	  "SELECT COUNT(X.id) AS cnt,MAX(X.stime) AS maxtime FROM ( SELECT id,UNIX_TIMESTAMP()-stamp AS stime FROM queue WHERE queue like ? AND status IN ('finished','failed','killed') ORDER BY stamp DESC LIMIT 100 ) AS X ",
	"queue.getqueuerate" =>
	  "SELECT COUNT(X.id) AS cnt,MAX(X.stime) AS maxtime FROM ( SELECT id,UNIX_TIMESTAMP()-stamp AS stime FROM queue WHERE queue like ? AND status IN ('queued') ORDER BY stamp DESC LIMIT 100 ) AS X",
	"queue.servicestatus" =>
	  "SELECT service,status,AVG(runtime) AS avgruntime,COUNT(*) AS cnt FROM queue GROUP BY service,status",
	"queue.userbreakdown" =>
	  "SELECT source,SUM(runtime) AS runtime,count(runtime) AS cnt FROM queue WHERE runtime IS NOT NULL GROUP BY source ORDER BY runtime DESC LIMIT 200"
};

sub new
{
	my $self  = {};
	my $class = shift;
	$self = bless($self, $class);
	$self->{options}  = shift;
	$self->{database} = \@_;
	my ($cpackage, $cfilename, $cline) = caller();
	try {
		$self->connect();
	  }
	  otherwise {
		my $what = shift;
		throw WebfaceSystemError
		  -text   => "Unable to open queue ($cfilename:$cline)",
		  -parent => $what;
	  };
	return $self;
}

sub connect
{
	my $self = shift;
	try {

		# silently attempt disconnectinf from old database.
		$self->{dbh}->disconnect() if (exists $self->{dbh});
	  }
	  otherwise {};
	try {
		$self->{dbh} =
		     DBI->connect(@{ $self->{database} }, { RaiseError => 1, AutoCommit => 1, AutoInactiveDestroy => 1 })
		  or throw WebfaceSystemError -text => "Unable to open queue database $self->{database}->[0]";

		my $sth = $self->{dbh}->prepare("SELECT CONNECTION_ID() AS threadid");
		$sth->execute();
		$self->{threadid} = $sth->fetchrow_hashref()->{threadid};

		# errorlog("QUEUE: connecting ($self->{threadid})");
	  }
	  otherwise {
		throw WebfaceSystemError
		  -text   => "Unable to open queue database $self->{database}->[0]",
		  -parent => shift;
	  };
	$self->{dbh}->{mysql_auto_reconnect} = 1;
}

sub sth
{
	my $self = shift;
	my $k    = shift;

	# Cached
	return $self->{sth}->{$k} if (exists $self->{sth}->{$k});

	#errorlog("sth : $sql->{$k}");

	throw WebfaceSystemError -text => "Statement '$k' not defined"
	  unless exists $sql->{$k};

	try {
		$self->{sth}->{$k} = $self->{dbh}->prepare($sql->{$k});
	  }
	  otherwise {
		throw WebfaceSystemError
		  -text   => "Unable to prepare queue database handle '$k' from '$sql->{$k}'",
		  -parent => shift;
	  };

	throw WebfaceSystemError -text => "Unable to prepare queue database handle '$k' from '$sql->{$k}'"
	  unless defined($self->{sth}->{$k});

	return $self->{sth}->{$k};
}

sub extract
{
	my $self = shift;
	my $q    = shift;
	my $opt  = shift;
	my @args = @_;
	my $result;

	my $sth = $self->sth($q);
	$sth->execute(@_);
	while (defined(my $row = $sth->fetchrow_hashref())) {
		if (exists $opt->{group}) {
			push @{ $result->{ $row->{ $opt->{group} } } }, $row;
		} else {
			push @{$result}, $row;
		}
	}
	return $result;
}

# Return queue configurations
sub config
{
	my $self  = shift;
	my $queue = shift;
	my %result;
	try {
		my $sth = $self->sth("sth.config");
		$sth->execute($queue);
		while (defined(my $idx = $sth->fetchrow_hashref)) {

			# in reality, one per queue
			$result{ $idx->{queue} } = $idx;
		}
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get queue configuration for $queue", -parent => shift;
	  };
	return %result;
}

# return list of queue(s), status and jobs
sub listqueue
{
	my $self     = shift;
	my $timeview = shift || 300;

	my %result;
	try {
		%result = $self->config('%');
		my $sth = $self->sth("queue.listqueue");
		$sth->execute('%', $timeview);

		while (defined(my $idx = $sth->fetchrow_hashref)) {
			$idx->{position} = ++($result{ $idx->{listqueue} }->{ $idx->{status} });
			push @{ $result{ $idx->{queue} }->{jobs} }, new Webface::job($idx);
		}
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get queue list", -parent => shift;
	  };
	return %result;
}

sub listqueuesource
{
	my $self     = shift;
	my $source   = shift;
	my $timeview = shift || 300;

	my %result;
	try {
		%result = $self->config('%');
		my $sth = $self->sth("queue.listqueuesource");
		$sth->execute('%', $source, $timeview);

		while (defined(my $idx = $sth->fetchrow_hashref)) {
			$idx->{position} = ++($result{ $idx->{listqueue} }->{ $idx->{status} });
			push @{ $result{ $idx->{queue} }->{jobs} }, new Webface::job($idx);
		}
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get queue list", -parent => shift;
	  };
	return %result;
}

sub statusqueue
{
	my $self = shift;
	my $timeview = shift || 300;
	my $sth;
	my $idx;
	my %result;
	try {
		%result = $self->config('%');
		foreach my $qn (keys %result, 'Summary') {
			$result{$qn}->{name}          = $qn;
			$result{$qn}->{active}        = 0;
			$result{$qn}->{queued}        = 0;
			$result{$qn}->{maxqueuedtime} = "NA";
			$result{$qn}->{avgqueuedtime} = "NA";
			$result{$qn}->{maxactivetime} = "NA";
			$result{$qn}->{avgactivetime} = "NA";
			$result{$qn}->{submitrate}    = "Idle";
			$result{$qn}->{finishrate}    = "Stalled";
		}

		$sth = $self->sth("queue.status");
		$sth->execute();
		while (defined(my $idx = $sth->fetchrow_hashref())) {
			$result{ $idx->{queue} }->{ $idx->{status} }        = $idx->{cnt};
			$result{ $idx->{queue} }->{"max$idx->{status}time"} = $idx->{maxtime};
			$result{ $idx->{queue} }->{"avg$idx->{status}time"} = $idx->{avgtime};
		}

		foreach my $qn (keys %result) {
			$sth = $self->sth("queue.getprocessrate");
			$sth->execute($qn);
			$idx = $sth->fetchrow_hashref();
			if ($idx && $idx->{maxtime} && $idx->{cnt} > 5) {
				$result{$qn}->{finishrate} = 60.0 * 60.0 * $idx->{cnt} / $idx->{maxtime};
			}
			$sth = $self->sth("queue.getqueuerate");
			$sth->execute($qn);
			$idx = $sth->fetchrow_hashref();
			if ($idx && $idx->{maxtime} && $idx->{cnt} > 5) {
				$result{$qn}->{submitrate} = 60.0 * 60.0 * $idx->{cnt} / $idx->{maxtime};
			}
			$result{$qn}->{finishrate} = "Idle"
			  if (  $result{$qn}->{submitrate} eq "Idle"
				and $result{$qn}->{finishrate} eq "Stalled");
			$result{$qn}->{submitrate} = "Empty"
			  if (  $result{$qn}->{submitrate} eq "Idle"
				and $result{$qn}->{active});
		}

		# Summary
		my $qn = 'Summary';
		$sth = $self->sth("queue.getprocessrate");
		$sth->execute('%');
		$idx = $sth->fetchrow_hashref();
		if ($idx && $idx->{maxtime} && $idx->{cnt} > 5) {
			$result{$qn}->{finishrate} = 60.0 * 60.0 * $idx->{cnt} / $idx->{maxtime};
		}

		$sth = $self->sth("queue.getqueuerate");
		$sth->execute('%');
		$idx = $sth->fetchrow_hashref();
		if ($idx && $idx->{maxtime} && $idx->{cnt} > 5) {
			$result{$qn}->{submitrate} = 60.0 * 60.0 * $idx->{cnt} / $idx->{maxtime};
		}

		$sth = $self->sth("queue.globalstatus");
		$sth->execute();
		while (defined(my $idx = $sth->fetchrow_hashref())) {
			$result{$qn}->{ $idx->{status} }        = $idx->{cnt};
			$result{$qn}->{"max$idx->{status}time"} = $idx->{maxtime};
			$result{$qn}->{"avg$idx->{status}time"} = $idx->{avgtime};
		}
		$result{$qn}->{finishrate} = "Idle"
		  if (  $result{$qn}->{submitrate} eq "Idle"
			and $result{$qn}->{finishrate} eq "Stalled");
		$result{$qn}->{submitrate} = "Empty"
		  if (  $result{$qn}->{submitrate} eq "Idle"
			and $result{$qn}->{active});
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get queue status", -parent => shift;
	  };
	return %result;
}

sub servicestatus
{
	my $self = shift;
	my $services;
	my $sth = $self->sth("queue.servicestatus");
	$sth->execute();
	while (defined(my $idx = $sth->fetchrow_hashref())) {
		$services->{ $idx->{service} }->{service} = $idx->{service};
		$services->{ $idx->{service} }->{ $idx->{status} } = $idx->{cnt};
		$services->{ $idx->{service} }->{avgruntime} = $idx->{avgruntime} if ($idx->{status} eq 'finished');
	}
	return $services;
}

sub queryconsumption
{
	my $self   = shift;
	my $source = shift;
	my $idx;
	try {
		my $sth = $self->sth("queue.queryconsumption");
		$sth->execute($source);
		$idx = $sth->fetchrow_hashref;
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get queue consumption for $source", -parent => shift;
	  };
	return $idx->{amount};
}

sub queryactive
{
	my $self   = shift;
	my $source = shift;
	my $result;
	try {
		my $sth = $self->sth("queue.queryactive");
		$sth->execute($source);
		while (defined(my $idx = $sth->fetchrow_hashref())) {
			$result->{ $idx->{queue} }->{ $idx->{source} } = $idx->{amount};
		}
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get active queue consumption for $source", -parent => shift;
	  };
	return $result;
}

sub lock
{
	my $self = shift;
	errorlog("Locking tables");
	try {
		$self->{dbh}->do("LOCK TABLE queue WRITE, quota WRITE, config READ, options READ");
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to lock queue", -parent => shift;
	  };
}

sub unlock
{
	my $self = shift;
	errorlog("UN-locking tables");
	try {
		$self->{dbh}->do("UNLOCK TABLES");
	  }
	  otherwise {

		#throw WebfaceSystemError -text => "Unable to unlock queue", -parent => shift;
		warn "Unable to unlock queue";
	  };
}

# add a job
sub addjob
{
	my $self = shift;
	my ($queue, $job, $source, $service) = @_;
	my %config = $self->config($queue);
	my $found;

	# make sure queue exists
	if (not exists $config{$queue}) {
		errorlog("Queue not defined");
		return new Webface::job(
			{
				jobid   => $job->{jobid},
				status  => 'rejected',
				message => "Queue '$queue' not defined"
			}
		);
	}

	# assumption: we don't care about config changes during insertion
	try {

		# get number of active,queued,from source
		my $sth = $self->sth("queue.listqueue");
		$sth->execute($queue, 300);
		while (defined(my $idx = $sth->fetchrow_hashref)) {
			++($config{$queue}->{ $idx->{status} });
			$found = $idx if ($idx->{jobid} eq $job->{jobid});
			$config{$queue}->{source}++
			  if ($idx->{source} eq $source && ($idx->{status} eq 'active' or $idx->{status} eq 'queued'));
		}
		$sth->finish;

		if (defined $found) {
			$job = new Webface::job(
				{
					jobid   => $job->{jobid},
					status  => 'rejected',
					message => "Job already in queue: $job->{jobid}"
				}
			);
		} elsif ($config{$queue}->{source} >= $config{$queue}->{maxqueue}) {
			$job = new Webface::job(
				{
					jobid   => $job->{jobid},
					status  => 'rejected',
					message => "Maximal number of jobs from $source ($config{$queue}->{maxqueue}) exceeded"
				}
			);
			$self->insertjob($job->{jobid}, $queue, $service, $source, undef, 'rejected',
				$config{$queue}->{expiration});
		} elsif ($job->{status} eq 'disabled') {
			$self->insertjob($job->{jobid}, $queue, $service, $source, undef, 'disabled',
				$config{$queue}->{expiration});
			$job = $self->getjob($job->{jobid});
		} else {

			$self->insertjob($job->{jobid}, $queue, $service, $source, undef, 'queued', $config{$queue}->{expiration});
			$job = $self->getjob($job->{jobid});
		}
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to add job $job->{jobid}", -parent => shift;
	  };
	return $job;
}

# Add job
sub insertjob
{
	my $self = shift;
	my ($jobid, $queue, $service, $source, $pid, $status, $expiration) = @_;
	try {
		my $sth = $self->sth("queue.insert");
		$sth->execute($jobid, $queue, $service, $source, $pid, $status, $expiration);
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to add job to queue ($jobid})", -parent => shift;
	  };
}

# set a job status
sub setstatus
{
	my $self      = shift;
	my $job       = shift;
	my $status    = shift;
	my $pid       = shift;
	my $oldstatus = shift || '%';
	my $rows;
	try {
		my $sth = $self->sth("queue.updatestatus");
		$sth->execute($status, $pid, $job->{jobid}, $oldstatus);
		$job->{status} = $status;
		$job->{pid}    = $pid;
		$rows          = $sth->rows;
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError
		  -text   => "Unable to set status '$status' for job $job->{jobid} ($oldstatus)",
		  -parent => shift;
	  };
	return $rows;
}

## set a job pid
#sub setpid
#{
#	my $self = shift;
#	my $job  = shift;
#	my $pid  = shift;
#	try {
#		my $sth = $self->sth("queue.updatepid");
#		$sth->execute($pid, $job->{jobid});
#		$job->{pid} = $pid;
#		$sth->finish;
#	  }
#	  otherwise {
#		throw WebfaceSystemError -text => "Unable to set pid for job $job->{jobid}", -parent => shift;
#	  };
#	return 1;
#}

sub updatejob
{
	my $self      = shift;
	my $job       = shift;
	my $status    = shift;
	my $runtime   = shift;
	my $rusage    = shift;
	my $expire    = shift;
	my $amount    = shift;
	my $oldstatus = shift || '%';
	my $rows;
	try {
		my $sth = $self->sth("queue.updatejob");
		$sth->execute($status, $runtime, $rusage, $expire, $amount, $job->{jobid}, $oldstatus);
		$job->{status}  = $status;
		$job->{runtime} = $runtime;
		$job->{rusage}  = $rusage;
		$job->{expire}  = $expire;
		$job->{amount}  = $amount;
		$rows           = $sth->rows;
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to update job $job->{jobid}", -parent => shift;
	  };
	return $rows;
}

# set a job email
sub setemail
{
	my $self  = shift;
	my $jobid = shift;
	my $email = shift;
	my $rows;
	try {
		my $sth = $self->sth("queue.updateemail");
		$sth->execute($email, $jobid);
		$rows = $sth->rows;
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to set email for job $jobid", -parent => shift;
	  };
	return $rows;
}

# get a job
sub getjob
{
	my $self  = shift;
	my $jobid = shift;
	my $found;
	my $job;
	try {
		my $sth = $self->sth("queue.get");
		$sth->execute($jobid);
		$job = $sth->fetchrow_hashref;
		$sth->finish;
		if (not defined $job) {
			$job->{jobid}  = $jobid;
			$job->{status} = 'expired';
		}
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to get job $jobid", -parent => shift;
	  };
	return new Webface::job($job);
}

# check and advance the queue
sub advance
{
	my $self   = shift;
	my %config = $self->config('%');
	my @activated;

	#errorlog("Advancing queue");

	try {
		my $active = $self->queryactive();

		# assumption: when we reach 'queued' entries,
		#  we have passed all 'active' entries, and know how many there are
		my $sth = $self->sth("queue.listqueue");
		$sth->execute('%', 300);
		my %activecnt;

		while (defined(my $idx = $sth->fetchrow_hashref)) {

			my $job = new Webface::job($idx);
			next if ($config{ $job->{queue} }->{state} ne 'scheduling');

			if ($job->{status} eq 'active') {

				#errorlog(
				#	"JOB: $job->{jobid} is $job->{status} for $job->{runtime} s where maxruntime is $config{ $job->{queue} }->{maxruntime}"
				#);

				# check if PID is alive
				if (!$job->checkpid()) {

					# the program was killed unexpectedly
					my $rows =
					  $self->updatejob($job, 'crashed', $job->{runtime}, undef, $config{ $job->{queue} }->{expiration},
						0, 'active');
					if ($rows) {
						errorlog(
							"Active job $job->{jobid} was killed due to the controlling process ($job->{pid}) was lost."
						);

						new Webface::page()->load("$self->{options}->{templates}/sys.crashed.mail")->report(
							$config{ $job->{queue} }->{maintainer},
							"[webface] Job killed due to missing process",
							{
								jobid => $job->{jobid},
								pid   => $job->{pid}
							}
						  )
						  if (defined($config{ $job->{queue} }->{maintainer}));
						errorlog("Done sending error report");
						if (defined $job->{email}) {
							$self->sendemail($job);
						}
					} else {
						errorlog("Job changed state while crash detect detected");
					}
					$activecnt{ $job->{queue} }--;
					next;
				}

				# check if max running time is expired
				if ($job->{runtime} > $config{ $job->{queue} }->{maxruntime}) {

					# The program exceeded maximal runtime
					my $rows =
					  $self->updatejob($job, 'killed', $job->{runtime}, undef, $config{ $job->{queue} }->{expiration},
						0, 'active');
					if ($rows) {
						my $procs = $job->kill();
						errorlog(
							"Killing pid group $job->{pid} ($procs) of job $job->{jobid} ($job->{service}) due to $job->{runtime} > $config{ $job->{queue} }->{maxruntime}"
						);

						# Gather the evidence
						my $service;
						my $submitter = "(no information)";
						my $workdir   = `ls -d1 $self->{options}->{servertmp}/*/$job->{jobid}`;
						chomp $workdir;
						my $files   = "No work directory found for job $job->{jobid}";
						my $command = "No work directory found for job $job->{jobid}";
						if (-d $workdir) {
							$files = `ls -la $workdir`;
							local $/ = undef;
							open(F, '<', "$workdir/command.dump");
							$command = <F>;
							close(F);

							my $filename = $job->servicefile($self->{options});
							try {

								# .. waitforfile($filename, 2);
								$service = load Webface::service($filename);

								#errorlog("Loaded service " . Dumper($service));
								$submitter = Dumper($service->{config}->{http});
							  }
							  otherwise {
								errorlog("Unable to load service '$filename'");
							  };
						}

						my $env = {
							jobid      => $job->{jobid},
							maxruntime => $config{ $job->{queue} }->{maxruntime},
							service    => $job->{service},
							queue      => $job->{queue},
							workdir    => $workdir,
							files      => $files,
							command    => $command,
							submitter  => $submitter
						};

						# send to queue maintainer
						new Webface::page()->load("$self->{options}->{templates}/sys.killed.mail")
						  ->report($config{ $job->{queue} }->{maintainer},
							"[webface] Job killed due to exceeded timelimit", $env)
						  if (defined($config{ $job->{queue} }->{maintainer}));

						# send to service maintainer
						if (   defined $service
							&& defined $service->{config}->{manager}
							&& $service->{config}->{manager} ne $config{ $job->{queue} }->{maintainer})
						{
							new Webface::page()->load("$self->{options}->{templates}/sys.killed.mail")
							  ->report($service->{config}->{manager},
								"[webface] Job killed due to exceeded timelimit", $env)
							  if (defined($service->{config}->{manager}));
						}

						errorlog("Done sending error report");

						if (defined $job->{email}) {
							$self->sendemail($job);
						}
					} else {
						errorlog("Job changed state while timing out");
					}
					$activecnt{ $job->{queue} }--;
					next;
				}

				# count number of active
				$activecnt{ $job->{queue} }++;
			} elsif ($job->{status} eq 'queued') {

				#errorlog("JOB: $job->{jobid} is $job->{status}");

				# check if there are available active slots and promote
				if ($activecnt{ $job->{queue} } < $config{ $job->{queue} }->{maxactive}) {
					$active->{ $job->{queue} }->{ $job->{source} } += 0;
					if ($active->{ $job->{queue} }->{ $job->{source} } < $config{ $job->{queue} }->{maxsource}) {

						# Start by setting the PID to the server process.
						#  this is changed when the active jobs are later
						#  launched.
						my $rows = $self->setstatus($job, 'active', $$, 'queued');
						if ($rows) {
							errorlog("Setting job $job->{jobid} as 'active'");
							$activecnt{ $job->{queue} }++;
							$active->{ $job->{queue} }->{ $job->{source} } += 1;
							push @activated, $job;
						} else {
							$job = $self->getjob($job->{jobid});
							errorlog("Job $job->{jobid} changed state 'queued'=>'$job->{status}'");
						}
					} else {
						errorlog(
							"Held job $job->{source}\@$job->{queue} as $active->{$job->{queue}}->{$job->{source}} >= $config{ $job->{queue} }->{maxsource}"
						);
					}
				}
			} elsif ($job->{status} eq 'finished') {

				# ignore
			} elsif ($job->{status} eq 'rejected') {

				# ignore
			} elsif ($job->{status} eq 'crashed') {

				# ignore
			} elsif ($job->{status} eq 'sanitized') {

				# ignore
			} elsif ($job->{status} eq 'disabled') {

				# ignore
			} elsif ($job->{status} eq 'killed') {

				# ignore
			}
		}
		$sth->finish;
	  }
	  otherwise {
		my $what = shift;
		errorlog("Exception: " . Dumper($what));
		throw WebfaceSystemError -text => "Unable to advance queue", -parent => $what;
	  };

	errorlog("Advanced queue (" . scalar(@activated) . " activated)") if (scalar(@activated));

	return @activated;
}

sub sendemail
{
	my $self = shift;
	my $job  = shift;

	try {
		new Webface::page()->load("$self->{options}->{templates}/finished.mail")
		  ->report($job->{email}, '[webface] Job ' . $job->{status}, $job, $self->{options});
	  }
	  otherwise {
		my $e = shift;
		errorlog("Exception: Unable to email $job->{email} :" . $e);
	  };

}

sub expire
{
	my $self = shift;
	my $rows;

	#errorlog("Expire queue");
	try {
		my $sth1 = $self->sth("quota.expire");
		$sth1->execute();
		$sth1->finish;
		my $sth2 = $self->sth("queue.expire");
		$sth2->execute();
		$sth2->finish;
		$rows = $sth2->rows;
	  }
	  otherwise {
		my $what = shift;
		errorlog("Unable to expire queue" . $what);
	  };
	return $rows;
}

sub close
{
	my $self = shift;
	try {
		foreach my $sthk (keys %{ $self->{sth} }) {
			$self->{sth}->{$sthk}->finish();
			delete $self->{sth}->{$sthk};
		}
		$self->{dbh}->disconnect();
		delete $self->{dbh};

		# errorlog("QUEUE: disconenecting ($self->{threadid})");
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to disconnect from queue";
	  };
}

sub testquota
{
	my $self   = shift;
	my $source = shift;
	my $queue  = shift;

	my $record = $self->queryquota($source, $queue);

	errorlog(Dumper($record));

	return ('OK',     $record) if (not defined $record->{limit});
	return ('BANNED', $record) if (defined $record->{limit} and $record->{limit} <= 0);

	#TODO: Enable when we import the whitelists
	#return ('REJECTED',$record) if (defined $record->{consumption} and $record->{consumption} > $record->{limit});
	if (defined $record->{consumption} and $record->{consumption} > $record->{limit}) {
		errorlog("Would have rejected as consumption>limit ");
	}

	return ('OK', $record);
}

sub queryquota
{
	my $self   = shift;
	my $source = shift || 'local';

	my @source = split(/\./, $source);
	my @test;
	my $record;

	push @test, $source;
	if ($source =~ m/^[0-9\.]+$/) {
		foreach my $i (reverse(0 .. $#source - 1)) {
			my $name = join('.', @source[0 .. $i]) . ".%";
			push @test, $name;
		}
	} else {
		foreach my $i (1 .. $#source) {
			my $name = "%." . join('.', @source[$i .. $#source]);
			push @test, $name;
		}
	}
	foreach my $name (@test) {
		$record = $self->getquota($name);
		last if (defined $record);
	}

	$record = $self->createquota($source) unless defined $record;
	$record->{consumption} = $self->queryconsumption($record->{source});
	$record->{search}      = \@test;

	# not found, create it
	return $record;
}

sub getquota
{
	my $self   = shift;
	my $source = shift;
	my $record;
	try {
		my $sth = $self->sth("quota.get");
		$sth->execute($source);
		$record = $sth->fetchrow_hashref;
		$sth->finish;
	  }
	  otherwise {
		my $e = shift;
		throw WebfaceSystemError -text => "Unable to get quota for $source ($e)", -parent => $e;
	  };
	return $record;
}

sub createquota
{
	my $self   = shift;
	my $source = shift;
	errorlog("queue.pm::createquota: $self->{options}->{maxquota} $self->{options}->{expirequota}");
	try {
		my $sth = $self->sth("quota.insert");
		errorlog("Creating quota: $source, $self->{options}->{maxquota}, $self->{options}->{expirequota}");
		$sth->execute($source, int($self->{options}->{maxquota}), int($self->{options}->{expirequota}));
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to create quota for $source", -parent => shift;
	  };
	return $self->getquota($source);
}

sub unban
{
	my $self   = shift;
	my $source = shift;
	try {
		errorlog("Un-banning $source");
		my $sth = $self->sth("quota.unban");
		$sth->execute($source);
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to unban $source", -parent => shift;
	  };
}

sub ban
{
	my $self   = shift;
	my $source = shift;
	try {
		errorlog("Banning $source");
		my $sth = $self->sth("quota.ban");
		$sth->execute($source);
		$sth->finish;
		$sth = $self->sth("quota.banupd");
		$sth->execute($source);
		$sth->finish;
		$sth = $self->sth("quota.banmv");
		$sth->execute($source);
		$sth->finish;
	  }
	  otherwise {
		throw WebfaceSystemError -text => "Unable to ban $source", -parent => shift;
	  };
}

sub pushbnw
{
	my $self      = shift;
	my $blacklist = shift;
	my $whitelist = shift;

	open(WLFILE, '<', $whitelist) or throw WebfaceSystemError -text => "Unable to open whitelist $whitelist";
	open(BLFILE, '<', $blacklist) or throw WebfaceSystemError -text => "Unable to open blacklist $blacklist";

	# We need exclusive access while we do this
	$self->{dbh}->do("LOCK TABLES quota WRITE");
	my $sth = $self->sth("quota.wipe");
	$sth->execute();
	$sth->finish();
	while (<WLFILE>) {
		chomp;
		my $host = $_;
		next if $host eq '';
		next if $host =~ m/^\s*$/;
		if ($host =~ m/^\..*/) {
			my $sth1 = $self->sth("quota.wipesource");
			$sth1->execute("%$host");
			my $sth2 = $self->sth("quota.push");
			$sth2->execute("%$host", undef);
		} else {
			my $sth1 = $self->sth("quota.wipesource");
			$sth1->execute("$host");
			my $sth2 = $self->sth("quota.push");
			$sth2->execute("$host", undef);
		}
	}
	while (<BLFILE>) {
		chomp;
		my $host = $_;
		next if $host eq '';
		next if $host =~ m/^\s*$/;
		if ($host =~ m/^\..*/) {
			my $sth1 = $self->sth("quota.wipesource");
			$sth1->execute("%$host");
			my $sth2 = $self->sth("quota.push");
			$sth2->execute("%$host", 0);
		} else {
			my $sth1 = $self->sth("quota.wipesource");
			$sth1->execute("$host");
			my $sth2 = $self->sth("quota.push");
			$sth2->execute("$host", 0);
		}
	}
	close(WLFILE);
	close(BLFILE);
	$self->{dbh}->do("UNLOCK TABLES");
}
1;
