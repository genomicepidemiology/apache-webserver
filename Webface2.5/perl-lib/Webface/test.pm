use strict;
use Error;

# this module is used to perform tests;

# list of current places to throw tests faults
my %messages=
(
	'Webface::queue-001'=>"opening queues",
	'Webface::queue-002'=>"preparing handles",
	'Webface::queue-003'=>"getting list",
#	t002=>"",
);

sub send {
	my ($test,$class)=@_;
	my($package, $filename, $line) = caller;
	my $id="$package-$test";
	throw $class "$package: $filename:$line $messages{$id}" if (exists $messages{$id}); 	
}
