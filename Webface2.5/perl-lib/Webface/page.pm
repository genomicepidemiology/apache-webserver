package Webface::page;
use strict;

use Error qw(:try);
use Webface::error;
use Webface::errorlog;
use HTML::Entities;
use CGI qw(:standard);
use Data::Dumper;
use Mail::Sender;

use vars qw/$config/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
}
my $smtp = $config->{'smtp'};
my $mail_from = $config->{'mail_from'};

# Render as whole page (with http headers)
#  new Webface::page("/path/filename",@_)
#  new Webface::page()->header()->load("/path/filename")->render(@_)
#
# Render partial (no http headers)
#  new Webface::page()->load("/path/filename")->render(\%env);
#
# Render partial from buffers (no http headers, no STDOUT)
#  new Webface::page()->silent()->set("message")->render(\%env);
#
# Render partial from buffers and email
#  new Webface::page()->silent()->set("message")->report(\%env);
#
# The syntax is supposed to be very DJANGO - like.
#
# {% <perlexpression> %}
# {%!include file%}
# {%!inject <perlexpression> %}
#
# for future: #####################################
# {%!if <perlexpression> %}
#  ...
# {%!elseif <perlexpression> %}
# ...
# {%!else <perlexpression> %}
# ...
# {%!endif%}
#
# {%!for $var in <perlexpression> %}
# ...
# {%!empty%}
# ...
# {%!endfor%}
#
## Render a page from a template

sub new
{

	# used for writing my $result=new page($file,$arg1,$arg2..)
	my $self  = {};
	my $class = shift;
	$self = bless $self, $class;
	$self->{maxincludes} = 100;    # dirty hack to prevent infinite loops.
	my $file = shift;
	local $| = 1;
	if (defined $file) {
		return $self->header()->load($file)->render(@_);
	} else {
		$self->{path} = '.';
		return $self;
	}
}

# stop echoing to STDOUT
sub silent
{
	my $self = shift;
	$self->{silent} = 1;
	return $self;
}

# print html header
sub header
{
	my $self = shift;
	print STDOUT CGI::header(@_) unless (exists $self->{silent});
	return $self;
}

# render content
sub render
{
	my $self = shift;
	my %__env;
	my $page;

	throw WebfaceSystemError -text => "No template page loaded" unless defined($self->{page});

	# collect all variables and functions from argument array
	foreach my $arg (@_) {

		# we will ignore any arguments that are not hash refs
		try {
			foreach my $key (keys %{$arg}) {

				# '-text' becomes 'text'
				my $ek = $key;
				$ek =~ s/^[^a-zA-Z]+//;
				$__env{$ek} = $arg->{$key};
			}
		};
	}

	#todo: does this make sense?
	#Hmmm... seems it does, we should add ourselves to the __env
	$__env{self} = $self;

	my @parts = split(/(\{\%.*?\%\})/, $self->{page});
	$self->{page} = undef;

	foreach my $part (@parts) {

		# Substitute text
		foreach my $var (keys %__env) {
			$part =~ s/\$$var/\$__env\{$var\}/gi;
		}
		if ($part =~ m/^\{\%\!(\S+)\s+(.*?)\%\}$/) {
			if ($1 eq 'include') {
				my $subpage = $2;
				$self->{includes}++;
				throw WebfaceConfigError -text => "Too many includes including $subpage"
				  if ($self->{includes} > $self->{maxincludes});
				$subpage = "$self->{path}/$subpage" unless ($subpage =~ m/^\//);

				# even through we load a new file, the @parts still lives in this frame.
				$part = $self->load($subpage)->render(\%__env);
			} elsif ($1 eq 'inject') {

				# inject
				try {
					#Webface::errorlog("running eval on: \$part=$2;");
					eval("\$part=$2;");
				  }
				  otherwise {
					throw WebfaceConfigError
					  -text   => "Error evaluating inject pragma '$1'",
					  -parent => shift;
				  };
				print STDOUT $part unless (exists $self->{silent});

			} elsif ($1 eq 'html') {
				try {
					eval("\$part=$2;");
				  }
				  otherwise {
					throw WebfaceConfigError
					  -text   => "Error evaluating print html pragma '$2'",
					  -parent => shift;
				  };
				print STDOUT $part unless (exists $self->{silent});
			} else {
				throw WebfaceConfigError -text => "Unknown pragma $1";
			}
		} elsif ($part =~ m/^\{\%(.*?)\%\}$/) {
			try {
				eval("\$part=HTML::Entities::encode($1);");
			  }
			  otherwise {
				throw WebfaceConfigError
				  -text   => "Error evaluating pragma '$1'",
				  -parent => shift;
			  };
			print STDOUT $part unless (exists $self->{silent});
		} else {
			print STDOUT $part unless (exists $self->{silent});
		}
		$page .= $part;
	}
	return $page;
}

# set content
sub set
{
	my $self = shift;
	my $page = shift;
	$self->{page} = $page;
	return $self;
}

# load content
sub load
{
	my $self = shift;
	my $file = shift;
	$self->{path} = $file;
	$self->{path} =~ s/^(.*)\/.*$/$1/;
	open(FILE, '<', $file) or throw WebfaceConfigError -text => "Unable to open template file $file : $!";
	while (<FILE>) { $self->{page} .= $_; }
	close(FILE) or throw WebfaceConfigError -text => "Unable to close template file $file : $!";
	return $self;
}

# render the page as an email, and send it.
sub report
{
	my $self    = shift;
	my $email   = shift;
	my $subject = shift;

	errorlog("Spawning email sender thread");
	my $childpid = fork();
	throw WebfaceConfigError -text => "Unable to fork email sending." unless defined $childpid;
	
	if (!$childpid) {
		errorlog("Emailing to $email");		
		my $message = $self->silent()->render(@_);
		my $sender  = new Mail::Sender({ smtp => $smtp, from => $mail_from });
		my $ret     = $sender->MailMsg(
			{
				from    => $mail_from,
				to      => $email,
				subject => $subject,
				msg     => $message
			}
		);
		if (ref($ret) ne 'Mail::Sender') {
			errorlog("Unable to send message to $email : $Mail::Sender::Error");
		}else{
			errorlog("Email send to $email");		
		}
		exit(0);
	}
	errorlog("Spawned email sender thread");
	return $self;
}

sub testsub
{
	my $self     = shift;
	my $whatever = shift;
	Webface::errorlog("$whatever");
}

# static error function
sub error
{
	my $message = HTML::Entities::encode(shift);
	print STDOUT CGI::header(-nph => 0)
	  . CGI::start_html('Webface internal error')
	  . CGI::h1('We have problems')
	  . CGI::p('The services queue has experienced internal problems.')
	  . CGI::p('The message returned is:')
	  . CGI::pre($message)
	  . CGI::end_html();
}
1;
