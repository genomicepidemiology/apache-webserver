package Webface::options;
use strict;
use Error qw(:try);
use DBI;
use Data::Dumper;
use Webface::errorlog;

sub new
{
	my $self  = {};
	my $class = shift;
	$self = bless $self, $class;
	$self->{database} = \@_;
	
	try {
		my $dbh = DBI->connect(@{ $self->{database} }, { RaiseError => 1, AutoCommit => 0, AutoInactiveDestroy=>1 })
		  or throw WebfaceSystemError -text => "Unable to open database $self->{database}->[0]";
		  
		my $sth = $dbh->prepare("SELECT SQL_CACHE * FROM options");
		$sth->execute()
			or throw WebfaceSystemError -text => "Unable to read from options database";
		while (defined(my $idx = $sth->fetchrow_hashref)) {
			$self->{ $idx->{name} } = $idx->{value};
			#errorlog("Option logged: $idx->{name} = $idx->{value}");
		}
		$sth->finish;
		$dbh->disconnect;
		undef $dbh;
	}
	otherwise {
		throw WebfaceSystemError -text => "Unable to get options from database",
			-parent => shift;
	};
	return $self;
}
1;
