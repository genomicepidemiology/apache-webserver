package Webface::job;
use strict;
use Error qw(:try);
use DBI;
use Data::Dumper;

sub new
{
	my $class = shift;
	my $self  = {};
	$self = bless $self, $class;
	my $status = shift;
	if (defined $status && (ref($status) eq '' || not defined ref($status)) ) {

		# its not a reference, then its the status
		$self->{status} = $status if defined $status;
	} elsif (defined $status && ref($status) eq 'HASH') {
		# transfer all SCALAR key/value pairs
		foreach my $k (keys %{$status}) {
			$self->{$k}=$status->{$k} if (ref($status->{$k}) eq '');
		}
	}else{
		throw WebfaceSystemError -text=>"new Webface::job called with invalid argument : (".ref($status).")".Dumper($status);
	}
	$self->{jobid} = $self->newid() unless defined $self->{jobid};
	return $self;
}

sub newid
{
	my $self = shift;
	return sprintf("%08X%08X%08X", time, $$, int(rand(2**(4 * 8))));
}

sub kill 
{
	my $self    = shift;
	my $signn   = 'TERM'; # not KILL?
	return 0 unless defined $self->{pid};
	# comprehensive killing of entire program group (negative pid)
	return CORE::kill $signn, -($self->{pid}),$self->{pid};
	#return CORE::kill(-1* 'KILL', $self->{pid});
}

# check that a process exists
sub checkpid
{
	my $self = shift;
	return 1 unless exists ($self->{pid});  # OK
	return 1 unless defined ($self->{pid}); # OK
	return CORE::kill 0, ($self->{pid});
}

sub outfile
{
	my $self    = shift;
	my $options = shift;
	throw WebfaceSystemError -text=>"option webfacetmp not defined" unless defined $options->{webfacetmp};
	return "$options->{webfacetmp}/$self->{jobid}.html";
}

sub servicefile
{
	my $self    = shift;
	my $options = shift;
	throw WebfaceSystemError -text=>"option webfacetmp not defined" unless defined $options->{webfacetmp};
	return "$options->{webfacetmp}/$self->{jobid}.service";
}

sub runoutfile
{
	my $self    = shift;
	my $options = shift;
	throw WebfaceSystemError -text=>"option webfacetmp not defined" unless defined $options->{webfacetmp};
	return "$options->{webfacetmp}/run.$self->{jobid}.html";
}

sub configfile
{
	my $self    = shift;
	my $options = shift;
	throw WebfaceSystemError -text=>"option webfacetmp not defined" unless defined $options->{webfacetmp};
	return "$options->{webfacetmp}/$self->{jobid}.cf";
}
1;
