package Webface::service;
use lib '../';
use strict;
use Error qw(:try);
use CGI qw(:standard);
use FCGI;
use Webface::page;
use Webface::filter;
use Webface::error;
use Webface::errorlog;
use Webface::config;
use Webface::uniwrap;
use Data::Dumper;
use BSD::Resource;
use Storable qw(freeze thaw);
use POSIX;
use POSIX 'setsid';

sub new
{
	my $self  = {};
	my $class = shift;
	$self = bless($self, $class);

	# Initialize
	$self->{job} = shift;
	my $request = shift;
	$self->{options}  = shift;
	$self->{config}   = shift;
	$self->{database} = \@_;
	$self->{seqcnt}   = 0;
	$self->{rescnt}   = 0;
	$self->{workdir}  = $self->mktmpdir();

	# get target files
	$self->{outfile}    = $self->{job}->outfile($self->{options});
	$self->{runoutfile} = $self->{job}->runoutfile($self->{options});
	$self->{configfile} = $self->{job}->configfile($self->{options});

	# link config file
	symlink($self->{config}->{file}, $self->{configfile})
	  or throw WebfaceSystemError "Unable to link config file $self->{config}->{file} -> $self->{configfile}: $!";

	# expand variables in config
	$self->expandvars();

	# parse options and copy data
	$self->parse($request);

	# create command array
	$self->buildcommandline();

	# Store the service dump
	$self->store();

	# im the parent
	return $self;
}

sub launch
{
	my $self = shift;
	my $fh   = shift;
	my $return;

	# spawn off service handler
	my $childpid = fork();
	throw WebfaceSystemError -text => "Unable to launch service thread : $!"
	  unless (defined $childpid);

	if (!$childpid) {
		try {

			# Rename process
			# If we do this, killall -USR1 webface2.fcgi won't affect the monitor processes.
			$0 =~ s/^.*\///;
			$0 =~ s/server/service/;

			# detatch process
			my $sid = POSIX::setsid();
			errorlog("Error when detatching service setsid(): $!") if ($sid == -1);
			
			#my $pgs = POSIX::setpgid(POSIX::getpid(), POSIX::getpid());
			#my $pgs = POSIX::setpgid(0,0);
			#errorlog("Error when detatching service setpgid(): $!") if (not defined $pgs);
	
			# completely detatch
			fork and exit;
			
			local $SIG{CHLD} = "DEFAULT";    # make child handling work default way
			local $SIG{USR1} = "DEFAULT";
			local $SIG{TERM} = "IGNORE";
			local $SIG{PIPE} = "DEFAULT";

			local $!;

			# we can't have the services maintain a lock on the server file
			close($fh) if defined($fh);

			# im the child!
			$Webface::errorlog::logfile = $self->{options}->{servicelog};
			
			errorlog("Launched child $$ $self->{job}->{pid} sid=$sid");

			# warn what were about to execute
			errorlog("Run child into $self->{outfile}");

			my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
			my $rows=$queue->setstatus($self->{job}, 'active',$$, 'active');
			$self->{job} = $queue->getjob($self->{job}->{jobid});
			$queue->close();
			undef $queue;

			if ($rows!=1) {
				throw WebfaceSystemError "Service: Job changed status $self->{job}->{jobid} 'active'=>'$self->{job}->{status}'";
			}

			throw WebfaceSystemError -text =>
			  "Launched service thread for job $self->{job}->{jobid} with status '$self->{job}->{status}')"
			  unless ($self->{job}->{status} eq 'active');

			try {
				$self->redir($self->{runoutfile});

				# Excellent way to debug commands
				open(CMD, '>', "$self->{workdir}/command.dump")
				  or throw WebfaceSystemError -text => "Unable to write command.dump : $!";
				print CMD Dumper($self->{command});
				print CMD "\n" . join(' ', @{ $self->{command} }) . "\n";
				close(CMD);

				# Sanitize arguemnts
				my $sanitized;
				foreach my $arg (@{ $self->{command} }) {
					if ($arg =~ /[\;\&\|\>\<\*\?\`\$\(\)\{\}\[\]\!\#]/) {
						errorlog("service::sanitized(($self->{job}->{jobid}) '$arg'");
						push @{ $self->{errors} },{ argument=>$arg };
					}
				}

				# Sanitize sequence filter errors
				if (exists $self->{errors}) {
					
					# Need to close db connections down because system() call kill them
					open(SANTITIZE, '>', $self->{outfile})
					  or throw WebfaceSystemError -text => "Unable to write $self->{outfile} : $!";
					#print SANTITIZE Dumper($self->{errors})."\n";
					print SANTITIZE "<ul>\n";
					foreach my $e ( @{ $self->{errors} }) {
						if (exists $e->{maxseq}) {
							print SANTITIZE "<li>Input exceeded maximal number of sequences : $e->{maxseq}</li>\n";
							delete $e->{maxseq};
						} 
						if (exists $e->{maxres}) {
							print SANTITIZE "<li>Input exceeded maximal number of residues for a given sequence : $e->{maxres}</li>\n";
							delete $e->{maxres};
						}
						if (exists $e->{maxsize}) {
							print SANTITIZE "<li>Input exceeded maximal total residues : $e->{maxsize}</li>\n";
							delete $e->{maxsize};
						}
						if (exists $e->{minres}) {
							print SANTITIZE "<li>Input sequence is smaller than minimally required : $e->{minres}</li>\n";
							delete $e->{minres};
						}
						if (exists $e->{argument}) {
							print SANTITIZE "<li>Argument contained illegal characters: '$e->{argument}' (avoid &quot;&amp;|&lt;&gt;;*?`\$(){}[]!#)</li>\n";
							delete $e->{argument};
						}
					}
					print SANTITIZE "</ul>";
					close(SANTITIZE)
					  or throw WebfaceSystemError -text => "Unable to write $self->{outfile} : $!";
					errorlog("$self->{outfile} : sanitized");
					errorlog(`ls -l $self->{outfile}`);

					my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
					$queue->setstatus($self->{job}, 'sanitized', undef);
					unlink($self->{runoutfile})
					  or throw WebfaceSystemError -text => "Unable to unlink $self->{runoutfile} : $!";
					$queue->close();
					close(STDOUT);
					close(STDERR);
					
				} else {

					# This has its own queue connection
					errorlog("service::start()");
					$self->{begintime} = time;
					$return            = $self->run();
					$self->{endtime}   = time;
				}

				errorlog("service::done()");

			  }
			  catch WebfaceConfigError with {
				my $what = Dumper(shift);
				errorlog("Exception (service): WebfaceConfigError:\n" . $what);

				Webface::page::error($what);
				$self->report('WebfaceConfigError', $what);
			  }
			  otherwise {
				my $what = Dumper(shift);

				# all other errors go here.
				errorlog("Exception (service): WebfaceSystemError:\n" . $what);

				Webface::page::error($what);
				$self->report('WebfaceSystemError', $what);
			  }
			  finally {

				# always make sure to register job status
				my $runtime;
				$runtime = $self->{endtime} - $self->{starttime}
				  if (exists $self->{endtime} and exists $self->{starttime});
				my $usage = getrusage(RUSAGE_CHILDREN);
				$self->updatejob($return, $runtime, $usage);
			  };
		  }
		  otherwise {
			errorlog("Exception (service): Unable to update job " . Dumper(shift));

			#$self->report('WebfaceSystemError', Dumper(shift));
		  };
		errorlog("Service ended");
		exit 0;
	}

	errorlog("Server Launched child $childpid");

}

sub buildcommandline
{
	my $self = shift;

	#This is where the service command is actually set
	push @{ $self->{command_line} }, split(/\s+/, $self->{config}->{command});
	push @{ $self->{command_line} }, split(/\s+/, $self->{config}->{options})
	  if (exists $self->{config}->{options});

	#	  errorlog("Config:\n".Dumper($self->{field}));

	foreach my $name (sort { $self->{field}{$a}->{line_no} <=> $self->{field}{$b}->{line_no} } keys %{ $self->{field} })
	{

		if ($self->{field}{$name}->{type} eq "fastapaste.seq") {
			next if (-s $self->{field}{$name}->{content} < 1)

			  # Added by dwe to test if files contains only newline. Used to filter away empty textareas
		} elsif ($self->{field}{$name}->{type} eq 'textarea') {
			next if (-s $self->{field}{$name}->{content} <= 1);

			# Added by dwe to ignore pulldown menues with have NO value
		} elsif ($self->{field}->{$name}->{type} eq 'pulldown') {
			next if length($self->{field}->{$name}->{content}) < 1;

			# Added by dwe to skip optionals with empty text
		} elsif ($self->{field}{$name}->{type} eq 'text') {
			if (length($self->{field}{$name}->{content}) < 1) {
				next;
			}
			$self->{field}{$name}->{content} =~ s/\s+$//;
			$self->{field}{$name}->{content} =~ s/^\s+//;
		} elsif ($self->{field}{$name}->{type} eq 'mselectionbox') {

		}

		if (exists $self->{field}{$name}->{content} and $self->{field}{$name}->{content} ne '') {
			push @{ $self->{command_line} }, $self->{field}->{$name}->{opt}
			  if (exists $self->{field}->{$name}->{opt});
			push @{ $self->{command_line} }, $self->{field}->{$name}->{content}
			  if (exists $self->{field}->{$name}->{value});
		} else {
			push @{ $self->{command_line} }, $self->{field}->{$name}->{default}
			  if (exists $self->{field}->{$name}->{default});
		}
	}

	#Push it to command array (where element 1 is bash executable)
	push @{ $self->{command} }, @{ $self->{command_line} };

}

sub expandvars
{
	my $self = shift;
	if (exists $self->{config}->{options}) {
		foreach my $var (keys %{ $self->{options} }) {
			$self->{config}->{options} =~ s/\$$var/$self->{options}->{$var}/gi
			  if (exists $self->{config}->{options});
			$self->{config}->{command} =~ s/\$$var/$self->{options}->{$var}/gi;
		}
		foreach my $var (keys %{ $self->{config} }) {
			next if ($var eq 'options');
			next if ($var eq 'command');
			next if ($var eq 'field');
			$self->{config}->{options} =~ s/\$$var/$self->{config}->{$var}/gi
			  if (exists $self->{config}->{options});
			$self->{config}->{command} =~ s/\$$var/$self->{config}->{$var}/gi;
		}
		foreach my $var (keys %{ $self->{job} }) {
			$self->{config}->{options} =~ s/\$$var/$self->{job}->{$var}/gi
			  if (exists $self->{config}->{options});
			$self->{config}->{command} =~ s/\$$var/$self->{job}->{$var}/gi;
		}
	}
}

sub parse
{
	my $self      = shift;
	my $request   = shift;
	my $filecount = 0;

	# initialize field hash
	foreach my $fld (@{ $self->{config}->{field} }) {
		$self->{field}->{ $fld->{name} } = $fld;
	}

	foreach my $name ($request->param) {
		next if ($name eq 'configfile');

		unless (exists $self->{field}->{$name}) {
			throw WebfaceConfigError -text => "Unhandled parameter '$name' in form $self->{config}->{http}->{referrer}";
		}
		my $type = $self->{field}->{$name}->{type};
		my $file = "$self->{workdir}/file.$filecount";
		if ($type =~ m/^(text|radio|checkbox|selectionbox|mselectionbox|pulldown)$/) {

			# Addition by dwe. Fixes an issue in which a mselectionbox would otherwise only pass the first value
			if ($type != 'mselectionbox') {
				$self->{field}->{$name}->{content} = $request->param($name);
			} else {
				$self->{field}->{$name}->{content} = join(",", $request->param($name));
			}

		} else {
			my $filter = new Webface::filter($self->{field}->{$name}->{filter});
			if ($type eq 'file') {

				# copy file literally
				my $fh = $request->upload($name);
				if ($fh) {
					$filter->copyfile($fh, $file);
					close($fh);
				} else {
					delete $self->{field}->{$name}->{content};
					next;
				}
			} elsif ($type eq 'textarea') {

				# make a file with the literal content
				$filter->copytext($request->param($name), $file);
			} elsif ($type eq 'fastafile') {

				# filter file into new file
				my $fh = $request->upload($name);
				if ($fh) {
					$filter->fastafile($fh, $file);
					close($fh);
					$self->{seqcnt} += $filter->{seqcnt};
					$self->{rescnt} += $filter->{sizecnt};
				} else {
					delete $self->{field}->{$name}->{content};
					next;
				}
			} elsif ($type eq 'fastapaste.seq') {

				# filter content into new file,
				#  using corresponding fastapaste.id
				my $id  = $request->param($self->{field}->{$name}->{id}->{name});
				my $seq = $request->param($name);
				$filter->fastapaste($id, $seq, $file);
				$self->{field}->{$name}->{id}->{content} = $file;
				$self->{seqcnt} += $filter->{seqcnt};
				$self->{rescnt} += $filter->{sizecnt};
			} elsif ($type eq 'fastapaste.id') {

				# this is handled in fastapaste.seq
			} else {
				throw WebfaceConfigError -text => "Unhandled filter config type: $type";
			}
			delete $filter->{map};
			#errorlog(Dumper($filter));
			if (exists $filter->{error}) {
				push @{$self->{errors}},$filter->{error};
			}
			$self->{field}->{$name}->{content} = $file;
			$filecount++;
		}
	}
	#errorlog(Dumper($self));
}

# for testing service
sub testrun
{
	my $self = shift;
	my $sec  = rand() * 5 + 5;

	errorlog("service::sleep($sec)");
	sleep($sec);
	print STDOUT header() . start_html("Hello world") . h1("dummy run") . pre(Dumper($self->{command})) . end_html();

	#	return 1;    # fail
	return 0;    # success
}

sub run
{
	my $self = shift;

	print STDOUT header();

	# Run the program.

	# display optional header
	if (exists $self->{config}->{header}) {
		new Webface::page()->load("$self->{config}->{header}")->render($self->{options}, $self->{config}, $self->{job});
	}
	close STDIN;
	$ENV{TMPDIR}=$self->{workdir} if (exists $self->{workdir});
	open(OLDERR, ">&", \*STDERR);
	select OLDERR;    # Just to avoid warning of it not being used
	open STDERR, '>&STDOUT';
	my $return;
	
	# Arguments are sanitized by being elements in an array.
	#  they are therefore passed as-is to exec (system).
	$return = system(@{ $self->{command} });
	open(STDERR, ">&OLDERR");

	# display optional footer
	if (exists $self->{config}->{footer}) {
		new Webface::page()->load("$self->{config}->{footer}")->render($self->{options}, $self->{config}, $self->{job});
	}

#	# act on return value
#	unless ($return == 0) {
#			if ($? == -1) {
#				print "Failed to execute : $!\n";
#			} elsif ($? & 127) {
#				printf("Died with signal %d, %s coredump\n", ($? & 127), ($? & 128) ? 'with' : 'without');
#			} else {
#				printf("Exited with value %d\n", $? >> 8);
#			}
#	}

	# clean up after execution
	#system("rm -rf $self->{workdir}") == 0 or throw WebfaceSystemError -text => "Unable to remove $self->{workdir} : $!";

	return $return;
}


sub rununiwrap {
	my $self = shift;

	my $uniwrap;

	sub sigh
	{	
		my $signame = shift;
		$uniwrap->interrupt() if (defined $uniwrap);
		throw WebfaceSystemError(-text=>"Interrupted");
	}

	print STDOUT header();

	# Run the program.

	# display optional header
	if (exists $self->{config}->{header}) {
		new Webface::page()->load("$self->{config}->{header}")->render($self->{options}, $self->{config}, $self->{job});
	}

	$ENV{TMPDIR}=$self->{workdir} if (exists $self->{workdir});
	open(OLDERR, ">&", \*STDERR);
	select OLDERR;    # Just to avoid warning of it not being used
	open STDERR, '>&STDOUT';
	my $return;
	                          
	if ($self->{command}->[0] =~ m/\/uniwrap/){
		# Hotwire buildin uniwrap
		local $SIG{TERM} = \&sigh;
		local $SIG{HUP} = \&sigh;
		local $SIG{INT} = \&sigh;
		my $uw=shift @{$self->{command}};
		my %opt=Webface::uniwrap::args(@{$self->{command}});
		my $uniwrap = new Webface::uniwrap(%opt);
		$return = $uniwrap->execute(@{$opt{args}});
	}else{
		# Arguments are sanitized by being elements in an array.
		#  they are therefore passed as-is to exec.
		$return = system(@{ $self->{command} });
	}	
	open(STDERR, ">&OLDERR");

	# display optional footer
	if (exists $self->{config}->{footer}) {
		new Webface::page()->load("$self->{config}->{footer}")->render($self->{options}, $self->{config}, $self->{job});
	}

	# act on return value
	#unless ($return == 0) {
	#		if ($? == -1) {
	#			print "Failed to execute : $!\n";
	#		} elsif ($? & 127) {
	#			printf("Died with signal %d, %s coredump\n", ($? & 127), ($? & 128) ? 'with' : 'without');
	#		} else {
	#			printf("Exited with value %d\n", $? >> 8);
	#		}
	#}

	# clean up after execution
	#system("rm -rf $self->{workdir}") == 0 or throw WebfaceSystemError -text => "Unable to remove $self->{workdir} : $!";

	return $return;	
}


sub updatejob
{
	my $self    = shift;
	my $return  = shift;
	my $runtime = shift;

	local $Data::Dumper::Terse = 1;
	my $usage = Dumper(shift);

	my $queue;
	my %config;
	my $job;
	my $rows;
	
	try {
		$queue = new Webface::queue($self->{options}, @{ $self->{database} });

		$job    = $queue->getjob($self->{job}->{jobid});
		%config = $queue->config($job->{queue});
		my $status = 'unknown';
		$status = 'crashed'  if (not defined $return);
		$status = 'failed'   if (defined $return and $return != 0);
		$status = 'finished' if (defined $return and $return == 0);

		# now use count (1)
		my $amount = (defined $return) ? 1 : 0;

		# now move the outfile, unless its sanitized
		if ( -e $self->{runoutfile} ) {
			rename($self->{runoutfile}, $self->{outfile})
		  		or throw WebfaceSystemError -text => "Unable to move $self->{runoutfile} -> $self->{outfile} : $!";
			$rows = $queue->updatejob($job, $status, $job->{runtime}, $usage, $config{ $job->{queue} }->{expiration},
				$amount, 'active');
			$job = $queue->getjob($job->{jobid});
			if ($rows != 1) {
				errorlog("Service: Updating job $job->{jobid} failed 'active'=>'$job->{status}'");
			}
		}
		
	  }
	  finally {

		# make sure we unlock the tables
		$queue->close()  if (defined $queue);
	  };
	$self->log();
	if (defined $job and defined $job->{email} and $rows) {
		# Only send email, if job was updated
		$self->sendemail($job);
	}
}

# Redirect output
sub redir
{
	my $self = shift;
	my $file = shift;

	errorlog("Redirecting IO to $file");

	try {
		close(STDOUT);
		close(STDERR);
		open(OUT, ">", $file)
		  or throw WebfaceSystemError -text => "Unable to open output file '$file' : $!";
		*STDOUT = *OUT;
		*STDERR = *OUT;

	  }
	  otherwise {
		my $e = shift;
		throw WebfaceSystemError -text => "Unable to open redirect", -parent => $e;
	  };

	# autoflush
	select STDERR;
	$| = 1;
	select STDOUT;
	$| = 1;
}

sub report
{
	my $self    = shift;
	my $target  = shift;
	my $what    = shift;
	my $manager = $Webface::config::manager;

	if ($target eq 'WebfaceSystemError') {
		$manager = $self->{config}->{manager};
	} elsif ($target eq 'WebfaceConfigError') {
		$manager = $self->{options}->{manager};
	}

	new Webface::page()->set($what)->report($manager, '[webface] error')
	  if (defined $manager);
}

sub sendemail
{
	my $self = shift;
	my $job  = shift;

	try {
		new Webface::page()->load("$self->{options}->{templates}/finished.mail")
		  ->report($job->{email}, '[webface] Job finished', $job, $self->{options});
	  }
	  otherwise {
		my $e = shift;
		errorlog("Exception: Unable to email $job->{email} :" . $e);
	  };

}

# create temporary directory
sub mktmpdir
{
	my $self = shift;
	throw WebfaceConfigError -text => "No servertmp in options"
	  unless (exists $self->{options}->{servertmp});
	throw WebfaceConfigError -text => "No servername in config"
	  unless (exists $self->{config}->{servername});
	throw WebfaceConfigError -text => "No jobid in service" unless (exists $self->{job}->{jobid});

	# create service directory
	my $servicedir = "$self->{options}->{servertmp}/$self->{config}->{servername}";
	if (!-d "$servicedir") {
		mkdir("$servicedir")
		  or throw WebfaceSystemError -text => "Unable to create $servicedir : $!";
	}

	# create working directory
	my $workdir = "$servicedir/$self->{job}->{jobid}";
	if (-d $workdir) {
		system("rm -rf $workdir") == 0
		  or throw WebfaceSystemError -text => "Unable to remove old $workdir : $!";
	}
	mkdir("$workdir")
	  or throw WebfaceSystemError -text => "Unable to create $workdir : $!";
	return $workdir;
}

# log a job execution
sub log
{
	my $self       = shift;
	my $logoptions = shift;
	my $logfile    = "$self->{options}->{logdir}/$self->{config}->{logfile}";
	open(LOG, ">>", $logfile)
	  or throw WebfaceSystemError -text => "Unable to open $logfile : $!";
	my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);
	my $timestr = sprintf("%4d %3s %2d %02d:%02d:%02d", $year + 1900, $abbr[$mon], $mday, $hour, $min, $sec);
	my $logcommand = @{ $self->{command} }[0];
	$logcommand = join(' ', @{ $self->{command} }) if (defined $logoptions);
	$logcommand =~ s/\S+\/(\S+)/$1/g;
	print LOG sprintf(
		"%s\t%s,%s\t%d\t%d\t%s\t%s\n",
		$timestr,
		$self->{config}->{servername},
		$self->{job}->{jobid},
		$self->{seqcnt}, $self->{rescnt}, $self->{config}->{http}->{host}, $logcommand
	);
	close(LOG)
	  or throw WebfaceSystemError -text => "Unable to close $logfile : $!";
}

# load the service structure
sub load
{
	my $class    = shift;
	my $filename = shift;
	my $self;
	local $/ = undef;
	open(SERVICEFILE, '<', $filename)
	  or throw WebfaceSystemError -text => "Unable to read service to $filename $!";
	$self = <SERVICEFILE>;
	close(SERVICEFILE)
	  or throw WebfaceSystemError -text => "Unable to read service to $filename $!";
	try {
		$self = thaw($self);
	  }
	  otherwise {
		my $what = shift;
		throw WebfaceSystemError -text => "Unable to read service to $filename $!", -parent => $what;
	  };
	return $self;
}

# store the service structure
sub store
{
	my $self     = shift;
	my $filename = $self->{job}->servicefile($self->{options});
	my $frozen;
	errorlog("Storing service in $filename");
	try {
		$frozen = freeze($self);
	  }
	  otherwise {
		my $what = shift;
		throw WebfaceSystemError -text => "Unable to read service to $filename", -parent => $what;
	  };

	open(SERVICEFILE, '>', $filename)
	  or throw WebfaceSystemError -text => "Unable to write service to $filename $!";
	print SERVICEFILE $frozen;
	close(SERVICEFILE)
	  or throw WebfaceSystemError -text => "Unable to write service to $filename $!";
}
1;
