package Webface::config;
use Webface::errorlog;
use Data::Dumper;
use strict;
use Error qw(:try);

# Load Webface Config
use vars qw/$config/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
}

# static locations, if everything else fail
our $manager = $config->{'manager'};

sub new
{
	my $self  = {};
	my $class = shift;
	my $file  = shift;
	$self = bless($self, $class);
	$self->{file} = $file;
	$self->read($file);
	return $self;
}

#TODO delete when sure that new version works
#this is old version kept for debugging purposes
sub readoption_old
{
	my $self = shift;
	my $_    = shift;
	if (
		m/^(text|file|textarea|radio|checkbox|selectionbox|mselectionbox|pulldown|fastafile|fastapaste)\s+(\S+)\s+(\S+)\s+(VALUE|NOVALUE)(?:\s+default\s+(\S+))?(?:\s+FILTER\s+(\S+))?(?:\s+(\S+))?/
	  )
	{

		#errorlog($_);
		my $opt;
		throw WebfaceConfigError -text => "Repeated option '$2'" if (exists $self->{used}{$2});

		#errorlog("Group 1: \"" . $1 . "\"\n");
		#errorlog("Group 2: \"" . $2 . "\"\n");
		#errorlog("Group 3: \"" . $3 . "\"\n");
		#errorlog("Group 4: \"" . $4 . "\"\n");
		#errorlog("Group 5: \"" . $5 . "\"\n");
		#errorlog("Group 6: \"" . $6 . "\"\n");
		#errorlog("Group 7: \"" . $7 . "\"\n");
		#errorlog("Group 8: \"" . $8 . "\"\n");

		$self->{used}{$2} = 1;
		$opt->{type}      = $1;
		$opt->{name}      = $2;
		$opt->{opt}   = $3     unless ($3 eq 'EMPTY');
		$opt->{value} = 'show' unless ($4 eq 'NOVALUE');
		$opt->{default} = $5 if (defined $5 and $5 ne '');
		$opt->{filter} = $6 if (defined $6);
		push @{ $self->{field} }, $opt;

		if ($1 eq 'fastapaste') {

			# idname,idseq
			my @name = split(/,/, $opt->{name});
			throw WebfaceConfigError -text => "Illegal fastapaste name"
			  unless (defined $name[0] && defined $name[1]);
			$opt->{type} = 'fastapaste.seq';
			$opt->{name} = $name[1];
			my $optid;
			$optid->{type} = 'fastapaste.id';
			$optid->{name} = $name[0];
			$optid->{seq}  = $opt;
			push @{ $self->{field} }, $optid;
			$opt->{id} = $optid;
		}
		return $opt;
	}
	return undef;
}

#New version, correctly handles cases whrere default is decleared but empty
sub readoption
{
	my $self    = shift;
	my $line_no = shift;
	my $_       = shift;
	if (
		#m/^(text|file|textarea|radio|checkbox|selectionbox|mselectionbox|pulldown|fastafile|fastapaste)\s+(\S+)\s+(\S+)\s+(VALUE|NOVALUE)(?:\s+default\s+(\S+))?(?:\s+FILTER\s+(\S+))?/
		m/^(text|file|textarea|radio|checkbox|selectionbox|mselectionbox|pulldown|fastafile|fastapaste)\s+(\S+)\s+(\S+)\s+(VALUE|NOVALUE)(.*)/

	  )
	{

		#errorlog("####!!!!!!! ".$_."\n");
		#errorlog("###############READING: " . $_ );
		#errorlog("Group 1: \"" . $1 . "\"\n");
		#errorlog("Group 2: \"" . $2 . "\"\n");
		#errorlog("Group 3: \"" . $3 . "\"\n");
		#errorlog("Group 4: \"" . $4 . "\"\n");
		#errorlog("Group 5: \"" . $5 . "\"\n");
		#errorlog("Group 6: \"" . $6 . "\"\n");

		my $type_string  = $1;
		my $name_string  = $2;
		my $opt_string   = $3;
		my $value_string = $4;
		my $filter_string;
		my $default_string;
		my $optionals = $5;

		#errorlog("OPTIONAL: " . $optionals);

		#		if($optionals =~ m/\s+(?:default\s?(\S?))(?:\s+FILTER\s+(\S+))/){
		#			$default_string = $1 if (defined $1 and $1 ne '');
		#			$filter_string = $2 if (defined $2);
		#			#errorlog("DEFAULT + FILTER");
		#		} elsif ($optionals =~ m/\s+(?:default\s?(\S?))/){
		#			$default_string = $1 if (defined $1 and $1 ne '');
		#			$filter_string = "";
		#			#errorlog("DEFAULT");
		#		} else {
		#			#errorlog("ELSE");
		#		}

		if ($optionals =~ m/\s+(?:default\s?(\S+)?)(?:\s+FILTER\s+(\S+))/) {
			$default_string = $1 if (defined $1 and $1 ne '');
			$filter_string  = $2 if (defined $2);

			#errorlog("DEFAULT + FILTER");
		} elsif ($optionals =~ m/\s+(?:default\s?(\S+)?)/) {
			$default_string = $1 if (defined $1 and $1 ne '');
			$filter_string  = "";

			#errorlog("DEFAULT");
		} else {

			#errorlog("ELSE");
		}

		#errorlog("Type: \"" . $type_string . "\"\n");
		#errorlog("Name: \"" . $name_string . "\"\n");
		#errorlog("Option: \"" . $opt_string . "\"\n");
		#errorlog("Value: \"" . $value_string . "\"\n");
		#errorlog("Default: \"" . $default_string . "\"\n");
		#errorlog("Filter: \"" . $filter_string . "\"\n");

		my $opt;
		throw WebfaceConfigError -text => "Repeated option '$name_string'" if (exists $self->{used}{$name_string});

		$self->{used}{$name_string} = 1;
		$opt->{type}                = $type_string;
		$opt->{name}                = $name_string;
		$opt->{opt}   = $opt_string unless ($opt_string   eq 'EMPTY');
		$opt->{value} = 'show'      unless ($value_string eq 'NOVALUE');
		$opt->{line_no} = $line_no;

		$opt->{default} = $default_string if (defined $default_string);
		$opt->{filter}  = $filter_string  if (defined $filter_string);
		push @{ $self->{field} }, $opt;

		if ($type_string eq 'fastapaste') {

			# idname,idseq
			my @name = split(/,/, $opt->{name});
			throw WebfaceConfigError -text => "Illegal fastapaste name"
			  unless (defined $name[0] && defined $name[1]);
			$opt->{type} = 'fastapaste.seq';
			$opt->{name} = $name[1];

			#errorlog("OPTION NAME: \"" . $name[1] . "\"");
			my $optid;
			$optid->{type}    = 'fastapaste.id';
			$optid->{name}    = $name[0];
			$optid->{seq}     = $opt;
			$optid->{line_no} = $line_no;
			push @{ $self->{field} }, $optid;
			$opt->{id} = $optid;
		}
		return $opt;
	}
	return undef;
}

sub read
{
	my $self = shift;
	my $file = shift;
	open(FILE, "<", $file) or throw WebfaceConfigError -text => "Unable to open configfile '$file'";
	my $line_no = 0;
	while (<FILE>) {
		$line_no += 1;
		chomp($_);
		next if /^\s*#/;
		next if /^\s*$/;
		$_ =~ s/</&lt;/g;
		$_ =~ s/>/&gt;/g;
		if (
			m/^(servername|manager|tmpdir|logfile|debug|logoptions|calcpage|queuepage|expiredpage|disabledpage|mailpage|queuefile|header|footer|fields|waittime):\s*(\S+)\s*/
		  )
		{
			$self->{$1} = $2;
			next;
		}
		if (m/^(command|options):\s*(.*)\s*$/) {
			$self->{$1} = $2;
			next;
		}
		next if (defined $self->readoption($line_no, $_));

		# illegal config line
		throw WebfaceConfigError -text => "Unable to parse config line: '$_'\n";
	}

	unless (defined $self->{used}{SEQ} or defined $self->{used}{"SEQNAME,SEQ"}) {
		$self->readoption($line_no, 'fastapaste SEQNAME,SEQ EMPTY VALUE')
		  or throw WebfaceConfigError -text => "Cannot process internal SEQNAME,SEQ";
	}

	# always have a queue
	$self->{queuefile} = 'none' unless (defined $self->{queuefile});

	# get request info
	$self->{http}->{addr} = $ENV{REMOTE_ADDR} if (exists $ENV{REMOTE_ADDR});
	$self->{http}->{addr} = "nohost" unless (exists $self->{http}->{addr});
	$self->{http}->{host} = $ENV{REMOTE_HOST} if (exists $ENV{REMOTE_HOST} and $ENV{REMOTE_HOST} ne 'no-data');
	$self->{http}->{host} = $self->{http}->{addr}
	  unless (exists $self->{http}->{host});
	$self->{http}->{referrer}    = $ENV{HTTP_REFERER};
	$self->{http}->{useragent}   = $ENV{HTTP_USER_AGENT};
	$self->{http}->{contenttype} = $ENV{CONTENT_TYPE};
	close(FILE);

	# todo: better error messages
	my @mandatory = ('command', 'servername', 'tmpdir', 'queuefile');
	foreach my $name (@mandatory) {
		throw WebfaceConfigError -text => "'$name:' not specified in $file" unless (exists $self->{$name});
	}
}
1;
