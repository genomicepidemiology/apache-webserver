package Webface::admin;
use strict;
use Error qw(:try);
use DBI;
use Data::Dumper;
use Webface::errorlog;

# Load Webface Config
use vars qw/$config $key/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $key = $config->{"key"};
}

sub new
{
	my $class = shift;
	my %opt=@_;
	my $self  = \%opt;
	$self = bless $self, $class;
	$self->{key} = $key;
	return $self;
}

sub validate
{
	my $self=shift;
	return 0 unless defined $self;
	if (ref($self) eq 'Webface::admin') {
		return 1 if ( $self->{request}->param('key') eq $self->{key});
	}else{
		return 1 if $self eq $key;
	}
	return 0;
}

sub process
{
	my $self=shift;
	my $page =  $self->{request}->param('status');
	
	if ($page eq 'job') {
		$self->jobstatuspage($self->{request}->param('jobid') || undef);
	} elsif ($page eq 'source') {
		$self->sourcestatuspage($self->{request}->param('source'));
	} elsif ($page eq 'queue') {
		$self->queuestatuspage(
			$self->{request}->param('queue') || undef,
			$self->{request}->param('service') || undef, 
			$self->{request}->param('jobst') || undef
		);
	} else {
		$self->serverstatuspage();
	}	
}

sub jobstatuspage
{
	my $self  = shift;
	my $jobid = shift;
	my $env;

	my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
	my $job   = $queue->getjob($jobid);
	my $dir   = `ls -d1 $self->{options}->{servertmp}/*/$jobid`;
	$env->{arguments} = "no directory for $jobid";
	chomp $dir;
	if ($dir ne '') {
		local $/ = undef;
		open(F, '<', "$dir/command.dump");
		$env->{arguments} = <F>;
		close(F);
	}

	#$env->{arguments}="$dir/command.dump";
	$env->{files} = "no directory for $jobid";
	$env->{files} = `ls -la $dir` if ($dir ne '');
	$env->{dir}   = $dir;

	$env->{files} .= "\n"; 
	$env->{files} .= `ls -la $self->{options}->{webfacetmp}/*$jobid*`;
		
	$queue->close();
	return new Webface::page("$self->{options}->{templates}/jobstatus.html", $env, $job);
}


sub serverstatuspage
{
	my $self   = shift;
	my $queue  = new Webface::queue($self->{options}, @{ $self->{database} });
	my %result = $queue->statusqueue();
	my @tables;

	my $avgqueuedtimealert   = 5 * 60;    # 5 minutes
	my $maxruntimeratioalert = 0.8;
	my $queuefillratioalert  = 0.8;

	sub rendertime
	{
		my $t = shift;
		return $t unless ($t =~ m/^\d+/);
		my $ot = $t;
		$t = int($t);
		my $r;
		my $s = $t % 60;
		$s = "0$s" if ($s < 10);
		$r = $s;
		$t -= $s;
		$t /= 60;
		my $m = $t % 60;
		$m = "0$m"   if ($m < 10);
		$r = "$m:$r" if ($t);
		$t -= $m;
		$t /= 60;
		my $h = $t % 24;
		$r = "$h:$r" if ($t);
		$t -= $h;
		$t /= 24;
		my $d = $t;
		$r = "$d $r" if ($d);
		return "$r";
	}

	sub renderqueuetable
	{
		my $q     = shift;
		my $table = "";
		my %times;
		foreach my $k ('maxactivetime', 'avgactivetime', 'maxruntime', 'maxqueuedtime', 'avgqueuedtime', 'expiration') {
			$times{$k} = "";
			$times{$k} = rendertime($q->{$k}) if exists $q->{$k};
		}
		my $rc = "sqr";
		$table .= "<table class=\"sq\" style=\"width: 100%\">\n";
		if (exists $q->{maintainer}) {
			$table .=
			  "<tr class=\"sqr\"><th class=\"sqh\"><a href=\"?status=queue&queue=$q->{name}&key=$self->{key}\">$q->{name}</a></th><th class=\"sqh\">$q->{maintainer}</th></tr>\n";
		} else {
			$table .= "<tr class=\"sqr\"><th class=\"sqh\">$q->{name}</th><th class=\"sqh\">&nbsp;</th></tr>\n";
		}
		if (exists $q->{state}) {
			$table .= "<tr class=\"sqr\"><td class=\"sqc\">state</td><td class=\"sqc\">$q->{state}</td></tr>\n";
		}
		if (exists $q->{maxactive}) {
			$table .=
			  "<tr class=\"sqr\"><td class=\"sqc\">active</td><td class=\"sqc\">$q->{active} / $q->{maxactive} (srcmax:$q->{maxsource})</td></tr>\n";
		} else {
			$table .= "<tr class=\"sqr\"><td class=\"sqc\">active</td><td class=\"sqc\">$q->{active}</td></tr>\n";
		}
		$rc = "sqr";
		if (    exists $q->{maxruntime}
			and $q->{maxactivetime} ne "NA"
			and ($q->{maxactivetime} / $q->{maxruntime}) > $maxruntimeratioalert)
		{
			$rc = "sqra";
		}
		if (exists $q->{maxruntime}) {
			$table .=
			  "<tr class=\"$rc\"><td class=\"sqc\">activetime</td><td class=\"sqc\">$times{avgactivetime} (&lt;$times{maxactivetime}) / $times{maxruntime}</td></tr>\n";
		} else {
			$table .=
			  "<tr class=\"$rc\"><td class=\"sqc\">activetime</td><td class=\"sqc\">$times{avgactivetime} (&lt;$times{maxactivetime})</td></tr>\n";
		}
		$rc = "sqr";
		if ($q->{finishrate} eq "Stalled") {
			$rc = "sqra";
		}
		$table .=
		  "<tr class=\"$rc\"><td class=\"sqc\">throughput</td><td class=\"sqc\">$q->{finishrate} / h</td></tr>\n";
		if (exists $q->{maxqueue} and $q->{maxqueue} and ($q->{queued} / $q->{maxqueue}) > $queuefillratioalert) {
			$rc = "sqra";
		}
		if (exists $q->{maxqueue}) {
			$table .=
			  "<tr class=\"$rc\"><td class=\"sqc\">queued</td><td class=\"sqc\">$q->{queued} / $q->{maxqueue}</td></tr>\n";
		} else {
			$table .= "<tr class=\"$rc\"><td class=\"sqc\">queued</td><td class=\"sqc\">$q->{queued}</td></tr>\n";
		}
		$rc = "sqr";
		if ($q->{avgqueuedtime} ne "NA" and $q->{avgqueuedtime} > $avgqueuedtimealert) {
			$rc = "sqra";
		}
		$table .=
		  "<tr class=\"$rc\"><td class=\"sqc\">queuedtime</td><td class=\"sqc\">$times{avgqueuedtime} (&lt;$times{maxqueuedtime})</td></tr>\n";
		$rc = "sqr";
		if ($q->{submitrate} eq "Idle") {
			$rc = "sqri";
		}
		$table .=
		  "<tr class=\"$rc\"><td class=\"sqc\">queuerate</td><td class=\"sqc\">$q->{submitrate} / h</td></tr>\n";

		if (exists $q->{expiration}) {
			$table .= "<tr class=\"sqr\"><td class=\"sqc\">expire</td><td class=\"sqc\">$times{expiration}</td></tr>\n";
		}
		$table .= "</table></div><br>\n";
		return $table;
	}

	sub renderconfigtable
	{
		my $table;
		$table = "<table class=\"sq\">\n";
		$table .= "<tr class=\"sqr\"><th colspan=\"2\" class=\"sqh\">Config</th></tr>\n";
		foreach my $k (keys %{ $self->{options} }) {
			$table .= "<tr class=\"sqr\"><td class=\"sqc\">$k</td><td class=\"sqc\">$self->{options}->{$k}</td></tr>";
		}
		$table .= "</table>\n";
		return $table;
	}

	sub renderservicetable
	{
		my $services;
		my $table;
		$services=$queue->servicestatus();

		my @fields = (
			'service',  'avgruntime', 'active',  'queued',    'finished', 'failed',
			'rejected', 'killed',     'crashed', 'sanitized', 'banned', 'disabled'
		);
		$table = "<table class=\"sq\">\n";
		$table .= "<tr class=\"sqr\">";
		foreach my $f (@fields) {
			$table .= "<th class=\"sqh\">$f</th>";
		}
		$table .= "</tr>";

		foreach my $sn (sort keys %{$services}) {
			$table .= "<tr class=\"sqr\">";
			foreach my $f (@fields) {
				if ($f eq 'service') {
					$table .=
					  "<td class=\"sqc\"><a href=\"?status=queue&service=$sn&key=$self->{key}\">$services->{$sn}->{$f}</a></td>";
				} elsif ($f eq 'avgruntime') {
					$table .= "<td class=\"sqc\">$services->{$sn}->{$f}</td>";
				} elsif (exists $services->{$sn}->{$f}) {
					$table .=
					  "<td class=\"sqc\"><a href=\"?status=queue&service=$sn&jobst=$f&key=$self->{key}\">$services->{$sn}->{$f}</a></td>";
				} else {
					$table .= "<td class=\"sqc\">-</td>";
				}
			}
			$table .= "</tr>\n";
		}
		$table .= "</table>\n";
		return $table;
	}

	sub renderactivity
	{
		my @stamps;
		my @data;
		my $binsize    = 60 * 10;    # 10 minute jumps
		my $samplesize = 100;        # 100 samples

		my $sth = $queue->sth("queue.listqueue");
		$sth->execute('%');
		while (defined(my $idx = $sth->fetchrow_hashref())) {
			push @stamps, $idx->{stamp};
		}

		while (@stamps) {
			my @sub   = @stamps[0 .. $samplesize];
			my $atime = $sub[$samplesize];
			my $btime = $sub[0];
			$atime += 1 if ($atime == $btime);
			last unless defined $atime;
			my $rate = $samplesize / ($atime - $btime);
			push @data, [$atime, $rate];
			my $this = shift @stamps;

			while (@stamps and $this + $binsize > $stamps[0]) {
				shift @stamps;
			}
		}

		#	"queue.listqueue"  =>
		#	  "SELECT SQL_CACHE *,IF(runtime IS NULL AND status='active',UNIX_TIMESTAMP()-stamp,runtime) AS runtime,(expire<=NOW()) AS isexpired FROM queue WHERE queue LIKE ? AND UNIX_TIMESTAMP()-stamp<? OR status='active' OR status='queued' ORDER BY queue,status,stamp ASC",

		#		SELECT COUNT(X.id) AS cnt,MAX(X.stime) AS maxtime FROM
		#		( SELECT id,UNIX_TIMESTAMP()-stamp AS stime
		#		   FROM queue WHERE queue like ?
		#		   AND status IN ('finished','failed','killed')
		#		   ORDER BY stamp DESC LIMIT 100 ) AS X

	}

	sub renderusers 
	{
		my @quota=@_;
		my $table = "<table class=\"sq\">\n";
		$table .= "<tr class=\"sqr\">";
		$table .= "<th class=\"sqh\">source</th>";
		$table .= "<th class=\"sqh\">limit / 24 h</th>";
		$table .= "<th class=\"sqh\">expire</th>";
		$table .= "</tr>\n";
		foreach my $idx (@quota) {
			$table .= "<tr class=\"sqr\">";
			$table .= "<td class=\"sqc\"><a href=\"?status=source&key=$self->{key}&source=$idx->{source}\">$idx->{source}</a></td>";
			$table .= "<td class=\"sqc\">$idx->{limit}</td>";
			$table .= "<td class=\"sqc\">$idx->{expire}</td>";
			$table .= "</tr>\n";
		}
		$table.="</table>\n";
		return $table;
	}

	#https://www.designmycss.com/table-designer/w4
	foreach my $qn (sort keys %result) {
		next if ($qn eq 'Summary');
		my $table = renderqueuetable($result{$qn});
		push @tables, $table;
	}

	my $tw = 3;
	my @userarray = ("['Source','Resource (s)']");
	my $sth       = $queue->sth("queue.userbreakdown");
	$sth->execute();
	while (defined(my $idx = $sth->fetchrow_hashref())) {
		push @userarray, "['$idx->{source}',$idx->{runtime}]";
	}

	my $configtable = renderconfigtable();
	my $summarytable = renderqueuetable($result{Summary});

	my $queuestables = "<table>";
	while (@tables) {
		$queuestables .= "<tr>";
		for (my $i = 0 ; $i < $tw ; $i++) {
			my $t = shift @tables || "<nbsp>";
			$queuestables .= "<td style=\"vertical-align: top\">$t</td>";
		}
		$queuestables .= "</tr>\n";
	}
	$queuestables .= "</table>\n";

	my $servicestable = renderservicetable();

	my $sth = $queue->sth("quota.list");
	$sth->execute();
	my @quota;
	while (defined(my $idx = $sth->fetchrow_hashref())) {
		$idx->{expire} = 'never' unless defined $idx->{expire};
		if (defined $idx->{limit}) {
			$idx->{limit} = 'blocked' unless $idx->{limit}>0;
		}else{
			$idx->{limit} = 'unlimited';
		}
		push @quota,$idx;
	}
	my $blockedtable = renderusers(grep {$_->{limit} eq 'blocked'} @quota);
	my $unlimitedtable = renderusers(grep {$_->{limit} eq 'unlimited'} @quota);
	
	return new Webface::page("$self->{options}->{templates}/serverstatus.new.html",
		{
			config => $configtable,
			summary => $summarytable,
			queues => $queuestables,
			services => $servicestable,
			blockedusers => $blockedtable,
			unlimitedusers =>$unlimitedtable,
			userarray => join(",", @userarray) 
		});
}

sub renderjobtable
{
	my $self        = shift;
	my $queuename   = shift;
	my $servicename = shift;
	my $jobst       = shift;
	my %queue_hash  = @_;
	my @jobs;

	sub chopstr
	{
		my $s = shift;
		$s = substr($s, 0, 15) . "..." if (length($s) > 18);
		return $s;
	}
	
	my $tables = "<div><table class=\"sq\">";
	my $isFirstFromQueue;
	
	#my @keys = ('id','jobid','queue','service','source','email','status','amount','stamp','expire');
	#my @keys = ('id', 'jobid', 'service', 'source', 'email', 'status', 'runtime', 'stamp', 'expire');
	my @keys = ('id','jobid','queue','service','source','email','pid','status','amount','runtime','rusage','stamp','expire','delivered','geo');
	
	$isFirstFromQueue = 1;
	
	my @jobs;
	if (defined $queuename) {
		push @jobs, @{ $queue_hash{$queuename}->{'jobs'} }
		  if (exists $queue_hash{$queuename}->{'jobs'});
	} else {
		# Usually a single service is in a given queue,
		#   but it might be in more theoretically.
		foreach my $qn (keys %queue_hash) {
			next unless exists $queue_hash{$qn}->{'jobs'};
			push @jobs, grep { (not defined $jobst) or ($_->{status} eq $jobst) }
			  grep { (not defined $servicename) or ($_->{service} eq $servicename) } @{ $queue_hash{$qn}->{'jobs'} };
		}
	}
	
	#errorlog(" render jobs :");
	#errorlog("    qn:$queuename") if (defined $queuename);
	#errorlog("    st:$jobst") if (defined $jobst);
	#errorlog("    sv:$servicename") if (defined $servicename);
	#errorlog("        queue:".Dumper(\%queue_hash));

	for my $job (@jobs) {
		next if (defined $servicename and $servicename ne $job->{service});
		if ($isFirstFromQueue) {
			$tables = $tables . '<tr class="sqr">';
			for my $key (@keys) {
				$tables = $tables . '<th class="sqh">' . $key . '</th>';
			}
			$tables           = $tables . '</tr>';
			$isFirstFromQueue = 0;
		}
		$job->{stamp} = localtime($job->{stamp});
		$tables = $tables . '<tr class="sqr">';
		for my $key (@keys) {
			if ($key eq 'jobid') {
				$tables = $tables . "<td class=\"sqc\"><a href=\"?jobid=$job->{$key}\">$job->{$key}</a></td>";
			} elsif ($key eq 'id') {
				$tables = $tables
				  . "<td class=\"sqc\"><a href=\"?status=job&key=$self->{key}&jobid=$job->{jobid}\">$job->{$key}</a></td>";
			} elsif ($key eq 'source') {
				my $f = chopstr($job->{$key});
				$tables = $tables
				  . "<td class=\"sqc\" style=\"white-space: nowrap;\"><a href=\"?status=source&key=$self->{key}&source=$job->{$key}\">$f</a></td>";
			} elsif ($key eq 'email') {
				my $f = chopstr($job->{$key});
				$tables = $tables . "<td class=\"sqc\" style=\"white-space: nowrap;\">$f</td>";
			} else {
				$tables = $tables . "<td class=\"sqc\" style=\"white-space: nowrap;\">$job->{$key}</td>";
			}
		}
		$tables = $tables . '</tr>' . "\n";
	}

	$tables = $tables . '</table></div>' . "\n" . '<br>' . "\n\n\n";
}


sub sourcestatuspage
{
	my $self       = shift;
	my $source     = shift;
	my $request    = $self->{request};
	my $partsource = $source;
	my $timelimit = 60 * 60 * 24;
	my $whois;
	my %queue_hash;
	my $action;

	if ($source =~ m/^[0-9\.]+$/) {
		$partsource =~ s/^([0-9\.]+)\.\d+/$1\.%/;
		$whois = "https://who.is/whois-ip/ip-address/$source";
	} else {
		$partsource =~ s/^[^\.]+\.(.+)/%\.$1/;
		$whois = "https://who.is/whois/$source";
	}

	my $queue = new Webface::queue($self->{options}, @{ $self->{database} });

	if ($request->param('ban')) {
		$queue->ban($request->param('ban'));
	}
	if ($request->param('unban')) {
		$queue->unban($request->param('unban'));
	}
	my $quota = $queue->queryquota($source);

	%queue_hash = $queue->listqueuesource($partsource, $timelimit);
	
	my $jobst       = undef;
	my $servicename = undef;
	my $queuename   = undef;
	my $table       = $self->renderjobtable($queuename, $servicename, $jobst, %queue_hash);
	
	my $env         = {
		options    => Dumper($self->{options}),
		tables     => $table,
		key        => $self->{key},
		partsource => $partsource,
		source     => $source,
		whois      => $whois,
		quota      => Dumper($quota),
		jobs       => \%queue_hash
	};
	#errorlog("viewing $source".Dumper($env));
	$queue->close();
	undef $queue;
	return new Webface::page("$self->{options}->{templates}/sourcestatus.html", $env);
}

sub queuestatuspage
{
	my $self        = shift;
	my $queuename   = shift;
	my $servicename = shift;
	my $jobst       = shift;

	my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
	my $timelimit = 60 * 60 * 24;
	my %queue_hash;

	if (defined $queuename and not defined $jobst) {
		%queue_hash = $queue->listqueue();
	} else {
		%queue_hash = $queue->listqueue($timelimit);
	}

	my $tables = $self->renderjobtable($queuename, $servicename, $jobst, %queue_hash);

	my %queues = $queue->config('%');
	my $env = {
		options => Dumper($self->{options}),
		tables => $tables,
		queues => Dumper(\%queues)
	};
	$queue->close();
	undef $queue;
	return new Webface::page("$self->{options}->{templates}/queuestatus.html", $env);
}
1;
