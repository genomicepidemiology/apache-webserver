package Webface::errorlog;
use strict;
use warnings;
use Devel::StackTrace;

use vars qw/$config/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
}
our $log_dir = $config->{'log_dir'};

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(
	errorlog
);

our $logfile = "$log_dir/$config->{servicelog}";

sub errorlog
{
	my $what = shift;
	$what="<nothing>" unless defined $what;
	if(open(CRASH, ">>", $logfile)){ 
	select CRASH;
	print CRASH "[$$] : $what\n";
	close(CRASH);
	}else{
		my $trace = Devel::StackTrace->new;
		print STDERR $trace->as_string;
		die "Can't open file: '$logfile'\n$!\n$what\n";
	}
}
1;
