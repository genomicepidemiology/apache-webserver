use strict;
use Error;

package Error;
use Data::Dumper;

sub show
{
	my $e = shift;
	print Dumper($e);
}

sub html
{
	my $e   = shift;
	my $msg = "";
	$msg .= "<div class='exception'><table>";
	$msg .= "<tr><td>Exception:</td><td>" . ref($e) . "</td></tr>";
	$msg .= "<tr><td>Package:</td><td>" . $e->{ -package } . " : " . $e->{-line} . "</td></tr>"
	  if exists($e->{ -package });
	$msg .= "<tr><td>Message:</td><td>" . $e->{-text} . "</td></tr>" if exists($e->{-text});
	if (exists $e->{-parent}) {
		if ($e->isa('Error')) {
			$msg .= "<tr><td>Parent:</td><td>" . $e->{-parent}->html() . "</td></tr>";
		} else {
			$msg .= "<tr><td>Parent:</td><td>" . Dumper($e) . "</td></tr>";
		}
	}
	$msg .= "</table></div>";
	return $msg;
}

package WebfaceSystemError;
use base qw(Error);
1;

package WebfaceConfigError;
use base qw(Error);
1;

package WebfacePermissionError;
use base qw(Error);

sub html
{
	my $e   = shift;
	my $msg = "";
	$msg .= "<div class='exception'><table>";
	$msg .= "<tr><td>Exception:</td><td>" . ref($e) . "</td></tr>";
	$msg .= "<tr><td colspan='2'>" . $e->{-text} . "</td></tr>";
	$msg .= "</table></div>";
	return $msg;
}
1;
