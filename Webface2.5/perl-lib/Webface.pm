package Webface;
use strict;
use Error qw(:try);
use Carp;
use DBI;
use CGI qw(:standard);
use FCGI;
use Webface::options;
use Webface::error;
use Webface::errorlog;
use Webface::page;
use Webface::config;
use Webface::queue;
use Webface::job;
use Webface::service;
use Webface::admin;
use Data::Dumper;
use File::stat;
use JSON qw(to_json);
use Time::HiRes;
use Fcntl qw(SEEK_SET);
use POSIX 'setsid';
use Data::Structure::Util qw(unbless);
use Fcntl qw(:flock SEEK_END);
use POSIX;
use List::Util qw(sum);

use vars qw/$config/;
BEGIN { 
	use JSON::Parse qw(json_file_to_perl);
	our $config = json_file_to_perl('/configs/webface_config.json');
}
our $log_dir = $config->{'log_dir'};
our $run_dir = $config->{'run_dir'};

*FCGI::DESTROY = sub { };

$Data::Dumper::Terse = 1;

sub javascriptredirect
{
	my $url = shift;
	return script({ language => "JavaScript" }, "location.replace(\"$url\")");
}

sub new
{
	errorlog("Building Webface!");

	my $self  = {};
	my $class = shift;
	$self = bless $self, $class;
	$self->{starttime}    = time();
	$self->{maxrequests}  = 600;
	$self->{database}     = \@_;
	$self->{requestcount} = 0;
	$self->{serverpid}    = "$run_dir/pid.txt";
	$self->{serverlog}    = "$log_dir/server.log";
	try {
		$self->{options} = new Webface::options(@{ $self->{database} });
	  }
	  catch WebfaceSystemError with {
		$self->report('WebfaceSystemError', shift);
	  }
	  catch WebfaceConfigError with {
		$self->report('WebfaceConfigError', shift);
	  };
	$self->{exitrequest} = 0;
	$self->{cmd}=$0;
	
	#errorlog("Options logged: ".Dumper($self->{options}));
	
	# remove compatability names
	if ($0 =~ s/\/nph-webface$/\/webface2.fcgi/) {
		errorlog("Request using $self->{cmd}");
	}
	if ($0 =~ s/\/webface$/\/webface2.fcgi/){
		errorlog("Request using $self->{cmd}");		
	}
	return $self;
}

sub requestexit
{
	my $self   = shift;
	my $reason = shift;
	$self->{exitrequest} = 1;
	errorlog("Request exit ($reason)");
}

sub accept
{
	my $self        = shift;
	my $fcgirequest = shift;
	return undef unless $fcgirequest->Accept() >= 0;

	# this attaches itself to the newly created STDIN/STDOUT by Accept()
	CGI->_reset_globals;
	CGI->_setup_symbols(@CGI::SAVED_SYMBOLS) if @CGI::SAVED_SYMBOLS;
	return $CGI::Q = CGI::new();
}

sub requestloop
{
	my $self = shift;

	# This prevents a race condition in which the
	# parent's $request is destroyed, Finish()ing and
	# thus closing the socket/handles of both parent and
	# child.  Now, Finish() must be called explicitly as
	# it will not be called upon destruction of a request.

	*FCGI::DESTROY = sub { };

	# create request object
	my $fcgirequest = FCGI::Request();
	my $isfcgi      = $fcgirequest->IsFastCGI();
	errorlog("Not an FCGI request") unless $isfcgi;

	# request loop use FCGI directly, as we need to Detach() and Attach() it..
	while (my $request = $self->accept($fcgirequest)) {

		$self->{requestcount}++;

		try {
			if (defined $request->param('ajax')) {
				if ($request->param('jobid')) {
					$self->ajax({ action=>'jobstatus', jobid=>$request->param('jobid'), key=>$Webface::admin::key});
				}else{
					$self->ajax($request->Vars());
				}
			} elsif (defined $request->param('configfile')) {

				# submit job
				$self->submitjob($request);

				# launch server (if needed)
				$self->serverloop($fcgirequest);
			} elsif (defined $request->param('jobid') and defined $request->param('email')) {
				$self->setemail($request->param('jobid'), $request->param('email'));
				$self->queryjob($request);
			} elsif (defined $request->param('status')) {

				# launch server (if needed)
				$self->serverloop($fcgirequest);

				# status
				$self->serverstatus($request);
			} else {

				# query job
				$self->queryjob($request);
			}
		  }
		  catch WebfacePermissionError with {
			my $what = shift;
			errorlog("Exception: WebfacePermissionError: " . Dumper($what));
			print STDOUT CGI::header();
			print STDOUT "<html><head><title>Permission error</title></head><body>" . $what->html() . "</body></html>";
			$self->requestexit('exception');

		  }
		  catch WebfaceSystemError with {
			my $what = shift;
			errorlog("Exception: WebfaceSystemError: " . Dumper($what));
			print STDOUT CGI::header();
			print STDOUT "<html><head><title>Internal error</title></head><body>" . $what->html() . "</body></html>";
			$self->requestexit('exception');

		  }
		  catch WebfaceConfigError with {
			my $what = shift;
			errorlog("Exception: WebfaceConfigError: " . Dumper($what));
			print STDOUT CGI::header();
			print STDOUT "<html><head><title>Configuration error</title></head><body>"
			  . $what->html()
			  . "</body></html>";
			$self->requestexit('exception');
		  }
		  otherwise {
			my $what = Dumper(shift);
			errorlog("Exception: Ungraceful exit: $what");
			print STDOUT CGI::header();
			print STDOUT
			  "<html><head><title>Internal error</title></head><body><h1>Internal Error:</h1><pre>$what</pre></body></html>";
			$self->requestexit('exception');
		  };

		$fcgirequest->Finish();

		if (-M $ENV{SCRIPT_FILENAME} < 0) {

			# Autorestart
			errorlog("Reloading $ENV{SCRIPT_FILENAME}");
			$self->requestexit('filechange');
		}

		#		my $timeleft = $self->{starttime} - time() + $self->{maxtime};
		#		$self->requestexit('time-to-live') if ($timeleft < 0);

		#		$self->requestexit('max requests') if ($self->{requestcount}>$self->{maxrequests});

		undef $request;
		undef $self->{job};

		last if ($self->{exitrequest});
	}

	# Doesn't really work, unless its Apache that send the signals.
	#   if we stop, apache will loose one request attempting to
	#   communicate with the dead fcgi process.
	$fcgirequest->Finish()   if (defined $fcgirequest);
	$fcgirequest->LastCall() if (defined $fcgirequest);
	undef $fcgirequest;
	errorlog("Request loop ending after $self->{requestcount} requests.");
}

# submit a new job
sub submitjob
{
	my $self    = shift;
	my $request = shift;
	my $config  = new Webface::config($request->param('configfile'));
	my $queue   = new Webface::queue($self->{options}, @{ $self->{database} });

	if ($config->{disabledpage}) {

		# If the service is disabled, specify this.

		my $job      = new Webface::job('disabled');
		my $template = "$self->{options}->{templates}/$job->{status}.html";
		if (defined $config->{disabledpage}) {
			if (-e $config->{disabledpage} and -r $config->{disabledpage}) {
				$template = $config->{disabledpage} if (defined $config->{disabledpage});
			} else {
				errorlog("Template $config->{disabledpage} is missing or not readable");
			}
		}
		$job = $queue->addjob($config->{queuefile}, $job, $config->{http}->{host}, $config->{servername}, undef);
		new Webface::page($template, $self->{options}, $config, $job);

	} else {

		my ($status, $quotarecord) = $queue->testquota($config->{http}->{host});

		#print("my status is: " . $status)
		if ($status eq 'OK') {
			if (defined $config->{queuefile}) {

				# Insert into queue

				# We have race condition
				#   As we generate the files first, the cleanup script might
				#   pick them up and delete them before it detects the job
				#   in the queue.
				#   Therefore the cleanup script requires that any files

				# We generate the service directory first, and then attempt to add the
				#  job. The good thing here is that we avoid a race condition where
				#  the job gets processed before it has finished getting the data.
				#  The bad thing is that we get the data even if the job gets rejected.
				#  However this is cleaned out next time clean.pl sweep up.

				my $job     = new Webface::job('queued');
				my $service = new Webface::service($job, $request, $self->{options}, $config, @{ $self->{database} });

				$job = $queue->addjob($config->{queuefile}, $job, $config->{http}->{host}, $config->{servername}, $$);
				if ($job->{status} eq 'rejected') {
					new Webface::page("$self->{options}->{templates}/error.html",
						$job, $self->{options}, $config, $quotarecord);
					$queue->close();
					return;
				} else {
					print STDOUT redirect(
						-URL => "$self->{options}->{cgiexec}?jobid=$job->{jobid}&wait=$self->{options}->{waittime}");
				}
			} else {
				throw WebfaceSystemError -text => "You always need to run on a queue";
			}
		} elsif ($status eq 'BANNED') {

			# print banned page
			my $job        = new Webface::job('banned');
			my $expiration = 24 * 60 * 60;
			$queue->insertjob($job->{jobid}, $config->{queuefile}, $config->{servername}, $config->{http}->{host},
				$$, 'banned', $expiration);
			new Webface::page("$self->{options}->{templates}/banned.html",
				$self->{options}, $config, $job, { quota => $quotarecord });
		} elsif ($status eq 'REJECTED') {

			# print rejected page
			my $job        = new Webface::job('rejected');
			my $expiration = 24 * 60 * 60;
			$queue->insertjob($job->{jobid}, $config->{queuefile}, $config->{servername}, $config->{http}->{host},
				$$, 'rejected', $expiration);
			new Webface::page("$self->{options}->{templates}/rejected.html",
				$self->{options}, $config, $job, { quota => $quotarecord });
		}
	}
	$queue->close();
	undef $queue;
}

# query a new job
sub queryjob
{
	my $self    = shift;
	my $request = shift;
	my $job;
	my $jobid = $request->param('jobid');
	throw WebfaceConfigError -text => "Jobid not provided" unless (defined $jobid);

	my $queue = new Webface::queue($self->{options}, @{ $self->{database} });

	$job = $queue->getjob($jobid);
	my $outfile    = $job->outfile($self->{options});
	my $runoutfile = $job->runoutfile($self->{options});
	my $configfile = $job->configfile($self->{options});

	# This is the default page template for the status
	my $template = "$self->{options}->{templates}/$job->{status}.html";

	if ($job->{status} eq 'expired') {
		return new Webface::page($template, $job, $self->{options});
	}

	my $env = {
		self    => $self,
		queue   => $queue,
		options => $self->{options}
	};

	$env->{waittime} = $request->param('wait') unless defined $request->param('wait');
	$env->{waittime} = 300 if ($env->{waittime} > 300);

	# if files are missing, still report expired
	# todo: redundant? will Webface::config throw exception?
	#p: WEbface::config will throw, It is redundant
	if (not -e $configfile) {
		return new Webface::page("$self->{options}->{templates}/expired.html", $job, $self->{options}, $env);
	}

	# get config from job
	my $config;
	$config = new Webface::config($job->configfile($self->{options}));

	if (   $job->{status} eq 'rejected'
		or $job->{status} eq 'killed'
		or $job->{status} eq 'queued'
		or $job->{status} eq 'crashed')
	{
		my %qconfig = $queue->config($job->{queue});
		$env->{maxruntime} = $qconfig{ $job->{queue} }->{maxruntime};

		# Overwrite the template if specified in the configuration file.
		if (defined $config->{queuepage} and $job->{status} eq 'queued') {
			if (-e $config->{queuepage} and -r $config->{queuepage}) {
				$template = $config->{queuepage} if (defined $config->{queuepage});
			} else {
				errorlog("Template $config->{queuepage} is missing or not readable");
			}
		}
		return new Webface::page($template, $self->{options}, $config, $job, $env);
	} elsif ($job->{status} eq 'active') {

		# Overwrite the template if specified in the configuration file.
		if (defined $config->{calcpage}) {
			if (-e $config->{calcpage} and -r $config->{calcpage}) {
				$template = $config->{calcpage} if (defined $config->{calcpage});
			} else {
				errorlog("Template $config->{calcpage} is missing or not readable");
			}
		}

		# if we dont follow it, just report the status page
		return new Webface::page($template, $self->{options}, $config, $job, $env)
		  if (not defined $request->param('follow'));

		$self->waitforfile($runoutfile, 2);

		# we need to catch the signal and exit when user cuts connection, or instance will keep hanging.
		$self->{request} = undef;

		# its active, and we want to follow it
		system("tail -f -N +0 $runoutfile")
		  or throw WebfaceSystemError -text => "Unable to follow $outfile $!";
	} elsif ($job->{status} eq 'sanitized') {

		$self->waitforfile($outfile, 5);
		open(OutputFile, '<', "$outfile") or throw WebfaceSystemError -text => "Unable to open outfile $outfile";
		my @contentArray = <OutputFile>;
		close OutputFile;

		my $content = join("", @contentArray);
		$job->{content} = $content;
		new Webface::page($template, $job, $self->{options}, $config, $env);

	} elsif ($job->{status} eq 'failed') {

		#There are problems with simple file access, this is supposed to be a safe workaround
		$self->waitforfile($outfile, 5);
		open(OutputFile, '<', "$outfile") or throw WebfaceSystemError -text => "Unable to open outfile $outfile";
		my @contentArray = <OutputFile>;
		close OutputFile;

		my $content = join("", @contentArray);

		#The content should probably be stripped of html
		#get the body markers out of the content

		if ($content =~ m/^.*?body.*?>(.*)/ms) {
			$content = $1;
		}

		if ($content =~ m/^(.*)<\/body>.*/ms) {
			$content = $1;
		}

		#if any of <body> or </body> is missing it's apparently not html so we leave it

		$job->{content} = $content;
		new Webface::page($template, $job, $self->{options}, $config, $env);
	} elsif ($job->{status} eq 'finished') {

		#TODO: this is probably broken, all lines are concatenated
		#this is related to the webpage layout itself, this could be done smarter
		#but for legacy compatibility better left untouched

		$self->waitforfile($outfile, 5);
		open(PAGE, "<", $outfile) or throw WebfaceSystemError -text => "Unable to read $outfile $!";
		while (<PAGE>) {
			print STDOUT $_;
		}
		close(PAGE) or throw WebfaceSystemError -text => "Unable to read $outfile $!";
	} elsif ($job->{status} eq 'disabled') {
		my $template = "$self->{options}->{templates}/$job->{status}.html";
		if (defined $config->{disabledpage}) {
			if (-e $config->{disabledpage} and -r $config->{disabledpage}) {
				$template = $config->{disabledpage} if (defined $config->{disabledpage});
			} else {
				errorlog("Template $config->{disabledpage} is missing or not readable");
			}
		}		
	} else {
		new Webface::page("$self->{options}->{templates}/expired.html", $job, $self->{options}, $config, $env);
	}
	$queue->close();
	undef $queue;
}

sub report
{
	my $self    = shift;
	my $target  = shift;
	my $what    = shift;
	my $manager = $Webface::config::manager;

	errorlog("Webface report($what)");
	if ($target eq 'WebfaceSystemError') {
		$manager = $self->{config}->{manager};
	} elsif ($target eq 'WebfaceConfigError') {
		$manager = $self->{options}->{manager};
	}

	new Webface::page()->set($what)->report($manager, '[webface] error')
	  if (defined $manager);
}

sub serverstatus
{
	my $self    = shift;
	my $request = shift;
	my $admin   = new Webface::admin(
		request  => $request,
		options  => $self->{options},
		database => $self->{database}
	);

	if (not $admin->validate()) {
		errorlog("No valid key on server status");
		new Webface::page("$self->{options}->{templates}/security.html");
		return;
	}

	return $admin->process();
}

sub setemail
{
	my $self  = shift;
	my $jobid = shift;
	my $email = shift;
	$email =~ s/^mailto://;
	$email =~ s/^.* <(.*)>/$1/;
	$email =~ s/[\\\/\(\)\{\}\s\[\]\<\>]//g;
	my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
	$queue->setemail($jobid, $email);
	$queue->close();
	undef $queue;
}

sub waitforfile
{
	my $self     = shift;
	my $file     = shift;
	my $maxtime  = shift || 2;
	my $thistime = 0;
	return if (-e $file);

	while (!-e $file) {
		errorlog("Waiting for file $file");
		$self->sleepwell(1);
		$thistime++;
		last if ($thistime >= $maxtime);
	}
	throw WebfaceSystemError -text => "Unable to open outfile $file ($maxtime sec)" unless (-e $file);
	errorlog("Found file $file") if ($thistime);
}

sub ajax
{
	my $self  = shift;
	my $request=shift;
	my $result;
		
	if (not Webface::admin::validate($request->{key})) {
		errorlog("No valid key on ajax status");
		$result={exception=>'Invalid key'};
	}elsif (not defined $request->{action}) {
		$result={exception=>'No action'}
	}elsif ($request->{action} eq 'jobstatus') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result   = $queue->getjob($request->{jobid});
		delete $result->{rusage};
		$queue->close();
		unbless($result);
		errorlog("Ajax: Check $request->{jobid}");
	}elsif ($request->{action} eq 'whitelist') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result=$queue->extract('quota.list.white',{});
		$queue->close();
	}elsif ($request->{action} eq 'blacklist') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result=$queue->extract('quota.list.black',{});
		$queue->close();
	}elsif ($request->{action} eq 'users') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result=$queue->extract('quota.list.users',{});
		$queue->close();
	}elsif ($request->{action} eq 'config') {
		$result=$self->{options};
	}elsif ($request->{action} eq 'queues') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result=$queue->statusqueue();
		$queue->close();
	}elsif ($request->{action} eq 'services') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result = $queue->servicestatus();
		$queue->close();
	}elsif ($request->{action} eq 'userbreakdown') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$result=$queue->extract('queue.userbreakdown',{});
		$queue->close();
	}elsif ($request->{action} eq 'queue') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		my %queues=$queue->listqueue();
		foreach my $qn (keys %queues) {
			next if (exists $request->{queue} and $request->{queue} ne $qn);
			next unless exists $queues{$qn}->{'jobs'};
			push @{$result->{jobs}}, 
			  grep { (not defined $request->{jobstatus}) or ($_->{status} eq $request->{jobstatus}) }
			  grep { (not defined $request->{service}) or ($_->{service} eq $request->{service}) } 
			  grep { (not defined $request->{source}) or ($_->{source} eq $request->{source}) } 
			  @{ $queues{$qn}->{'jobs'} };
		}
		$queue->close();
	}elsif ($request->{action} eq 'source') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		my $quota=$queue->queryquota($request->{source});
		my $partsource=$request->{source};
		my $whois;
		if ($partsource =~ m/^[0-9\.]+$/) {
			$partsource =~ s/^([0-9\.]+)\.\d+/$1\.%/;
			$whois = "https://who.is/whois-ip/ip-address/$request->{source}";
		} else {
			$partsource =~ s/^[^\.]+\.(.+)/%\.$1/;
			$whois = "https://who.is/whois/$request->{source}";
		}
		$result = { quota=>$quota, source=>$request->{source}, partsource=>$partsource, whois=>$whois };
		$queue->close();
	}elsif ($request->{action} eq 'ban') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$queue->ban($self->{source});
		$queue->close();
		$result = { result=>'ok' };
	}elsif ($request->{action} eq 'unban') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$queue->unban($self->{source});
		$queue->close();
		$result = { result=>'ok' };
	}elsif ($request->{action} eq 'stopqueue') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$queue->stopqueue($self->{queue});
		$queue->close();
		$result = { result=>'ok' };
	}elsif ($request->{action} eq 'startqueue') {
		my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
		$queue->startqueue($self->{queue});
		$queue->close();
		$result = { result=>'ok' };
	}elsif ($request->{action} eq 'file') {
		# TODO: file content for job $request->{jobid} $request->{file}
		$result = { result=>'ok', file=>'not yet' };
	}
	print STDOUT CGI::header('application/json');
	print STDOUT to_json($result);
	
}

sub sleepwell
{
	my $self      = shift;
	my $sleeptime = shift;
	my $count     = 0;
	my $end       = Time::HiRes::time() + $sleeptime;
	#errorlog("Server sleepwell ($sleeptime) ...");
	for (; ;) {
		local $!;
		my $delta = $end - Time::HiRes::time();
		last if $delta <= 0;
		my $fd = select(undef, undef, undef, $delta);
		if ($fd < 1) {
			#errorlog("  sleepwell fail : $!") if ($?);
		}
		if ($count++) {
			#errorlog("  sleepwell.. ( $delta )");
		}
	}
	my $delta = Time::HiRes::time() - $end;
	if ($delta > 1) {
		#errorlog(".. woke up (+$delta)");
	} else {
		#errorlog(".. woke up");
	}
}

# This function is a server loop. It acts as a server to advance the
#  queue every few seconds.

sub serverloop
{
	my $self        = shift;
	my $fcgirequest = shift;
	my $sleeptime   = 2;

	# Attempt to make a nonblocking lock on the webface server lock file
	my $fh;
	open($fh, '>>', $self->{serverpid})
	  or throw WebfaceSystemError -text => "Unable to open server lock file '$self->{serverpid}' : $!";

	my $locked = flock($fh, LOCK_EX | LOCK_NB);

	# If we get the lock, the server must be missing for whatever reason
	#  and we restart it.
	if ($locked) {

		# If we get the lock, it menas that there is no server, and we
		#  must launch a new one to control queue progression.

		# Detatch FCGI connection
		$fcgirequest->Detach() if (defined $fcgirequest);

		my $childpid = fork();
		throw WebfaceSystemError -text => "Unable to launch server thread : $!"
		  unless (defined $childpid);

		if (!$childpid) {
			
			# Deamonize
			my $sid= POSIX::setsid();
			errorlog("Error when detatching server setsid(): $!") if ($sid == -1);
					
			#my $pgs = POSIX::setpgid(POSIX::getpid(), POSIX::getpid());
			#my $pgs = POSIX::setpgid(0,0);
			#errorlog("Error when detatching server setpgid(): $!") if (not defined $pgs);					

			# completely detatch
			fork and exit;

			# rename process
			$0 =~ s/^.*\///;
			$0 =~ s/fcgi/server/;

			undef $fcgirequest if (defined $fcgirequest);    # destroy request handle.

			try {


				local $!;
				local $SIG{USR1} = "DEFAULT";
				local $SIG{PIPE} = "DEFAULT";
				local $SIG{TERM} = sub {
					local $!;
					errorlog("Server request exit");
					$self->{exitrequest} = 1;
				};
				local $SIG{CHLD} = 'IGNORE';

				$Webface::errorlog::logfile = $self->{serverlog};

				errorlog("##################################");
				errorlog("Launched server as $$ sid=$sid");

				# Make sure we get our PID in there
				select $fh;
				$| = 1;
				truncate $fh, 0;
				seek($fh, 0, SEEK_SET);
				print $fh "$$";

				try {
					close(STDOUT);
					close(STDERR);
					open(OUT, ">>", $Webface::errorlog::logfile)
					  or throw WebfaceSystemError -text =>
					  "Unable to open output file '$Webface::errorlog::logfile' : $!";
					select OUT;
					$|      = 1;
					*STDOUT = *OUT;
					*STDERR = *OUT;
					select STDOUT;
					$| = 1;
					select STDERR;
					$| = 1;
				  }
				  otherwise {
					my $e = shift;
					throw WebfaceSystemError -text => "Unable to open redirect", -parent => $e;
				  };

				while (1) {

					my @activejobs;
					$self->sleepwell($sleeptime);
					try {
						my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
						$queue->expire();
						@activejobs = $queue->advance();
						$queue->close();
					  }
					  otherwise {
						my $what = shift;
						errorlog("Exception: server caught " . $what);
					  };

					# Launch services for each activated job
					errorlog("Server launching " . scalar(@activejobs) . " jobs") if (scalar(@activejobs));
					foreach my $job (@activejobs) {
						## Create service from dump file
						try {
							errorlog("Launching $job->{jobid} ...");
							my $filename = $job->servicefile($self->{options});
							$self->waitforfile($filename, 2);
							my $service = load Webface::service($filename);
							$service->launch($fh);
							errorlog("... launched!");
						  }
						  otherwise {
							my $what = shift;
							errorlog("Exception: server caught : " . Dumper($what));
							my $queue = new Webface::queue($self->{options}, @{ $self->{database} });
							$queue->setstatus($job, 'crashed', undef);
							$queue->close();
						  };
					}
					#errorlog("Server done launching");

					if (-M $ENV{SCRIPT_FILENAME} < 0) {

						# Autorestart
						errorlog("Reloading $ENV{SCRIPT_FILENAME}");
						$self->requestexit('filechange');
					}

					last if $self->{exitrequest};
				}
			  }
			  otherwise {
				my $what = shift;
				errorlog("Server caught exception " . Dumper($what));
			  };

			flock($fh, LOCK_UN);
			close($fh);
			errorlog("Server ended");
			exit(0);
		}

		errorlog("Launched server as $childpid");

		# Attach FCGI connection
		$fcgirequest->Attach() if (defined $fcgirequest);

	} else {

		errorlog("Server already active");
	}

	# As the child and parent share FD and the lock, let the parent close
	#  the file handle so only the child (queue server) holds the lock.
	close($fh);
}
1;
