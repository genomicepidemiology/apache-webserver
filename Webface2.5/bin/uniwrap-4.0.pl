#!/usr/bin/env perl
use strict;
use Sys::Hostname;
use Error qw(:try);

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{"lib_dir"};
}

use lib "$lib_dir/";
use Webface::uniwrap;
use Webface::error;

my $host = hostname();
my $uniwrap;

sub sigh
{
	my $signame = shift;
	$uniwrap->interrupt() if (defined $uniwrap);
	throw WebfaceSystemError(-text=>"Interrupted");
}

$SIG{TERM} = \&sigh;
$SIG{HUP} = \&sigh;
$SIG{INT} = \&sigh;

# arguments =================================================================

my %opt=Webface::uniwrap::args(@ARGV);

# connect =================================================================
$uniwrap = new Webface::uniwrap(%opt);

# execute ================================================================
$uniwrap->execute(@{$opt{args}});

exit(0);
#EOF
