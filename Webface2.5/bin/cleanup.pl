#!/usr/bin/env perl
use strict;
use warnings;

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{"lib_dir"};
}

use lib "$lib_dir/";
use Webface::queue;
use Webface::options;

# Connect to Database
my $tmp_dir = $config->{"tmp_dir"};
my $conn = sprintf("DBI:mysql:%s:%s:%s", $config->{db}, $config->{host}, $config->{port});
my @database = ($conn, $config->{user}, $config->{password});

my $options=new Webface::options(@database);
my $queue=new Webface::queue($options,@database);

my $serverdir =  $options->{servertmp};
my $jobdir =  $options->{webfacetmp};
my $gracetime = (60*60);                   # Files/directories less that 1 hour old are kept
my $weektime = 7*24*60*60; 
my $verbose = shift @ARGV || 0;
#goto X;

my $rmfilecount=0;
my $rmdircount=0;
my $kpfilecount=0;
my $kpdircount=0;

foreach my $d ( `find $serverdir/*/ -type d` ) {
  chomp $d;
  my $id=$d;
  $id=substr($id,length($serverdir));
  $id =~s/\/.*\///;
  next if ($id eq '');
  unless ($id =~ m/^[0-9A-F]{24}$/){
  	warn("Malformed jobid $id from $d");
  	next;
  }
  my $job=$queue->getjob($id);
  
  if ($job->{status} eq 'expired' or $job->{status} eq 'rejected' ) {
    die("Illegal work directory '$d'") unless $d =~ m/^\Q$tmp_dir\E\//;
    my $mtime = (lstat($d))[9];
    if (defined $mtime and $mtime+$gracetime<time()) {
	    print STDERR "removing ($id) $d\n" if ($verbose);
    	system("rm -rf $d");
    	$rmdircount++;
    }else{
    	print STDERR "keeping recent ($id) $d\n" if ($verbose);
    	$kpdircount++;
    }
  }elsif($job->{status} eq 'killed' or $job->{status} eq 'crashed' ){
    die("Illegal work directory '$d'") unless $d =~ m/^\Q$tmp_dir\E\//;
  	my $mtime = (lstat($d))[9];
    if (defined $mtime and $mtime+$weektime<time()) {
	    print STDERR "removing ($id) $d\n" if ($verbose);
    	system("rm -rf $d");
    	$rmdircount++;
    }else{
    	print STDERR "keeping recent ($id) $d\n" if ($verbose);
    	$kpdircount++;
    }  	
  }else{
    print STDERR "keeping ($id) $d\n" if ($verbose);
    $kpdircount++;
  }
}

foreach my $d ( `find $jobdir/ -type f -o -type l` ) {
  chomp $d;
  my $id=$d;
  next if ($id =~ m/,/); # skip old type jobs
  $id=substr($id,length($jobdir)+1);
  $id =~ s/^run\.//;
  $id =~ s/\.(html|cf|service)$//;
  unless ($id =~ m/^[0-9A-F]{24}$/){
  	warn("Malformed jobid $id");
  	next;  	
  }
  my $job=$queue->getjob($id);
  if ($job->{status} eq 'expired' or $job->{status} eq 'crashed' or $job->{status} eq 'rejected') {
    die("Illegal work directory '$d'") unless $d =~ m/^\Q$tmp_dir\E\//;
    my $mtime = (lstat($d))[9];
    if (defined $mtime and $mtime+$gracetime<time()) {
	    print STDERR "removing ($id) $d\n" if ($verbose);
    	system("rm $d");
    	$rmfilecount++;
    }else{
    	print STDERR "keeping recent ($id) $d\n" if ($verbose);
    	$kpfilecount++;
    }
  }elsif($job->{status} eq 'killed' or $job->{status} eq 'crashed'){
    die("Illegal work directory '$d'") unless $d =~ m/^\Q$tmp_dir\E\//;
    my $mtime = (lstat($d))[9];
    if (defined $mtime and $mtime+$weektime<time()) {
	    print STDERR "removing ($id) $d\n" if ($verbose);
    	system("rm $d");
    	$rmfilecount++;
    }else{
    	print STDERR "keeping recent ($id) $d\n" if ($verbose);
    	$kpfilecount++;
    }
  }else{
    print STDERR "keeping ($id) $d\n" if ($verbose);
    $kpfilecount++;
  }
}

print STDERR "Removed $rmfilecount / Kept $kpfilecount job files\n";
print STDERR "Removed $rmdircount / kept $kpdircount job directories\n";
#EOF
