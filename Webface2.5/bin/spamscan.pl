#!/usr/bin/env perl
use strict;
use warnings;
use Data::Dumper;

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{'lib_dir'};
}

use lib "$lib_dir/";
use Webface::queue;
use Webface::options;

my @words = @ARGV;

die("Needs words to scan for") unless (@words);

# Connect to Database
our $key = $config->{'key'};
our $protocol = $config->{'protocol'};
our $host = $config->{'host'};
our $cgi_exe = $config->{'cgi_exe'};

my $conn = sprintf("DBI:mysql:%s:%s:%s", $config->{db}, $config->{host}, $config->{port});
my @database = ($conn, $config->{user}, $config->{password});

my $options=new Webface::options(@database);
my $queue=new Webface::queue($options,@database);

my $serverdir =  $options->{servertmp};
my $jobdir =  $options->{webfacetmp};

my @files = glob("$options->{servertmp}/*/*/file.*");
push @files, glob("$options->{servertmp}/*/*/command.dump");

warn("Scanning ".scalar(@files)." files...\n");
my %jobids;

foreach my $file ( @files ) {
	my $data=`head -c 4096 $file`;
	my $hit=1;
	my $jobid;
	if ( $file =~/.*\/([^\/]+)\/[^\/]+$/i ) {
		$jobid=$1;
	}
	if (not defined $jobid) {
		warn("\n$file unknown jobid\n");
	}
	foreach my $word ( @words ) {
		if ( $data =~ m/$word/) {
			$jobids{$jobid}=$file;
			last;
		}
	}
	print STDERR "." unless exists $jobids{$jobid};
	print STDERR "*" if 	exists $jobids{$jobid};
}

warn("\nScanning ".scalar(keys %jobids)." jobs...\n");
my %sources;
foreach my $jobid ( keys %jobids ) {
	my $job=$queue->getjob($jobid);
	if ( not defined $job or $job->{status} eq 'expired') {
		warn("$jobid not in queue\n");
		next;
	}
	if (not exists $job->{source} or not defined $job->{source}) {
		warn("$jobid has no source: ".Dumper($job));
		next;	
	}
	$sources{$job->{source}}=$jobids{$jobid};
}

warn("\nScanning ".scalar(keys %sources)." sources...\n");
foreach my $source ( keys %sources) {
	my $quota=$queue->queryquota($source);
	if (not defined $quota->{limit} or $quota->{limit} != 0) {
		warn("check $protocol://$host$cgi_exe?status=source&key=$key&source=$source\t$sources{$source}\n");
	}
}

#EOF
