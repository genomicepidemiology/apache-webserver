#!/usr/bin/env perl
use strict;
use warnings;
use Data::Dumper;

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{"lib_dir"};
}

use lib "$lib_dir/";

my $bin_dir = $config->{"bin_dir"};
my $tmp_dir = $config->{"tmp_dir"};

my $checktime = 600;

unless (@ARGV) {
	eval("use Webface::queue");
	eval("use Webface::options");
	
	# Connect to Database
	my $conn = sprintf("DBI:mysql:%s:%s:%s", $config->{db}, $config->{host}, $config->{port});
	my @database = ($conn, $config->{user}, $config->{password});
	
	my $options   = new Webface::options(@database);
	my $queue     = new Webface::queue($options, @database);
	my $serverdir = $options->{servertmp};
	my $jobdir    = $options->{webfacetmp};
	
	my $check = $config->{check};
	
	while (1) {
		warn "Checking resources\n";
		foreach my $arch (keys %$check) {
			my @where = ('-wraparch', $arch);
			@where = ('-wrapserver', $check->{$arch}->{server}) if (exists $check->{$arch}->{server});
			my $timeout = 120;
			eval {
				local $SIG{ALRM} = sub { die "Timeout $timeout\n" };
				open(TEST, '-|', "$bin_dir/uniwrap-4.0.pl", '-wrapprog', "$bin_dir/monitor.pl",
					@where, '-wraparch', $arch, '-t', $timeout, '-a', 90000000, @{ $check->{$arch}->{checks} })
				  or die("Cannot open test : $!");
				while (<TEST>) {
					chomp;
					last if (m/^Check OK$/);
					if (m/^Check failed/) {
						die($_);
					}
					my $psline = $_;
					if (m/cd \Q$tmp_dir\E\/server\/[^\/]+\/([0-9A-F]+)/) {
						my $job = $queue->getjob($1);
						if (not defined $job) {
							warn("Process job not found: $psline");
						}
						if ($job->{status} ne 'active') {
							warn("Process job $job->{status}: $psline");
						}
					}
				}
				close(TEST) or die("Cannot close test : $!");
				alarm(0);
			};
			alarm(0);
			if ($@) {
				warn "Resource $arch problem: $@\n";
				warn "Disabling queues\n";
				$queue->{dbh}->do("UPDATE config SET state='stopped' WHERE queue LIKE '$arch\.\%';");
			} else {
				warn "Resource $arch ok, scheduling queues\n";
				$queue->{dbh}->do("UPDATE config SET state='scheduling' WHERE queue LIKE '$arch\.\%';");
			}
		}
		sleep($checktime);
	}
} else {

	# -d dir -l loadmax% -a diskavaliable -w filewrite
	my $hostname = `hostname`;
	my $minavail = 1000000;
	my $loadmax  = 2;
	my $timeout  = 120;

	#warn "Reached $hostname";

	eval {
		local $SIG{ALRM} = sub { die "Timeout $timeout\n" };
		use strict;
		while (@ARGV) {
			my $opt = shift @ARGV;
			my $val = shift @ARGV;
			if (not defined $val) {
				die("Argument missing after $opt");
			}
			if ($opt eq '-d') {
				unless (-d $val) {
					die("Cannot stat $val");
				} else {

					#warn "Directory $val exists\n";
				}
				my $df = `df $val`;
				if ($?) {
					die("Cannot df $val");
					return -1;
				}
				my @dff = split(/[ \n\r]+/, $df);
				my $a = $dff[-3];
				if (defined $a) {

					#warn "Avail $val = $a\n";
				} else {
					die("Cannot parse df $val : $df");
				}
				if ($a < $minavail) {
					die("Filesystem of $val full ($a<$minavail)");
					return -1;
				}
			} elsif ($opt eq '-a') {
				$minavail = $val;
			} elsif ($opt eq '-l') {
				$loadmax = $val;
			} elsif ($opt eq '-t') {
				$timeout = $val;
				alarm($timeout);
			} elsif ($opt eq '-w') {
				my $file = `echo 'test' > $val`;
				if ($?) {
					die("Cannot write $val : $!");
				}
			} elsif ($opt eq '-r') {
				my $file = `cat $val`;
				if ($?) {
					die("Cannot read $val : $!");
				}

				#warn "File $val readable\n";
			} else {
				die("Unknown option $opt");
			}
		}

		#warn "Analyzing processlist\n";
		open(PS, 'ps -fu www |') or die("Unable to get processlist");
		while (<PS>) {
			print $_;

			#			chomp;
			#			my $psline=$_;
			#			if (m/cd \Q$tmp_dir\E\/server\/[^\/]+\/([0-9A-F]+)/) {
			#				my $job=$queue->getjob($1);
			#				if (not defined $job) {
			#					warn("Process job not found: $psline");
			#				}
			#				if ($job->{status} ne 'active') {
			#					warn("Process job $job->{status}: $psline");
			#				}
			#			}
		}
		close(PS);
		alarm(0);
	};
	if ($@) {
		print "Check failed : $@\n";
		exit -1;
	}
	print "Check OK\n";

}

exit 0;

#EOF
