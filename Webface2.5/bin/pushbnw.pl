#!/usr/bin/env perl
use strict;
use warnings;

use vars qw/$config $lib_dir/;
BEGIN { 
	use JSON::Parse 'json_file_to_perl';
	our $config = json_file_to_perl('/configs/webface_config.json');
	our $lib_dir = $config->{"lib_dir"};
}

use lib "$lib_dir/";
use Webface::queue;
use Webface::options;

# Connect to Database
my $conn = sprintf("DBI:mysql:%s:%s:%s", $config->{db}, $config->{host}, $config->{port});
my @database = ($conn, $config->{user}, $config->{password});

my $options=new Webface::options(@database);
my $queue=new Webface::queue($options,@database);

my $bwdir = "$options->{webfacehome}/access";
$queue->pushbnw(
"$bwdir/black.list",
"$bwdir/white.list"
);

#EOF
