#!/usr/bin/env perl

print "<h1>Webface testing server</h1>\n";
print "Executing $0\n";
print "<h2>Arguments</h2>\n";
print "<hr>\n";

foreach my $arg (@ARGV) {
  print $arg;
  print "\n<hr>\n";
}

print "<h2>Environment</h2>\n";
print "<hr>\n";
print "<table>\n";
foreach my $k (keys %ENV) {
  print "<tr><td>$k</td><td>=</td><td>$ENV{$k}</td></tr>\n";
}
print "</table>\n";

my $sleepnow;
my $exitnow;
my $exitcode=0;
foreach my $arg (@ARGV) {
  if ($sleepnow) {
    if ($arg<0) {
	    print "\n<h2>Echoing...</h2>\n";
		system("echo 'Hello World!'");
    }else{
	    print "\n<h2>Sleeping $arg seconds</h2>\n";
    	sleep($arg);
    }
  }
  if ($exitnow) {
    $exitcode=0+$arg;
  }
  undef $sleepnow;
  undef $exitnow;
  $sleepnow=$arg if ($arg eq '-computetime');
  $exitnow=$arg if ($arg eq '-exitcode');
}
print "\n<h2>Exitcode : $exitcode</h2>\n";    
exit($exitcode);
#EOF
