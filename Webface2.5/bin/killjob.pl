#!/usr/bin/env perl
my $jobid=shift @ARGV;

open(PS,'-|','ps','--forest','-fu','www') or die("$!");

my %pids;
my @tokill;
while(<PS>) {
  chomp;
  if (m/^\S+\s+(\d+)\s+(\d+).*$jobid/) {
     unless ($1 == 1) {
       $pids{$1}=1;
       #print "$_\n";
       next;
    } 
  }
  if (m/^\S+\s+(\d+)\s+(\d+)/) {
    if (exists $pids{$2} and $1 != 1) {
      $pids{$1}=1;
       push @tokill,$1;
      print "$_\n";
    }
 }
}

if (@tokill) {
  print "kill -TERM ".join(' ',@tokill)."\n";
}
