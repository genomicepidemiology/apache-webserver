
var oldstatus='none';
var checks=0;
var maxwaittime=20;
var waittime=0;
var url='';

function launchcheck(_oldstatus,_jobid,_url,_maxwaittime) {
	$('#progress').html('<img src="/images/ajax-loader.gif" />');
	oldstatus=_oldstatus;
	jobid=_jobid;
	url=_url;
	maxwaittime=_maxwaittime||20;
	check();
}

function reloadstatus() {
	location.reload();
}

function check() {
	$('#progress').html('<img src="/images/ajax-loader.gif" />  checking job status ...');
	$.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: { ajax: "1", jobid: jobid },
        success: function(job){
        	checks++;waittime++;
        	$('#progress').html('<img src="/images/ajax-loader.gif" /> processing job ('+waittime+')');
            if (job.status!=oldstatus) {
            	waittime=1;
            	$("[name='status']").html(job.status);
            	oldstatus=job.status;
            	if (job.status!='active' && 
            		job.status!='queued') {
            		location.reload();
            	}
            }
    		if (waittime>maxwaittime) {
    			waittime=maxwaittime;
    		}
    		setTimeout('check()', waittime*1000);
        },
        error: function(xhr, ajaxOptions, thrownError){
        	$('#progress').html("Error retrieving job status : "+xhr.status+" "+thrownError);
        	setTimeout('reloadstatus()', (waittime+3)*1000);
        },
        complete: function() {
        }
    });
}
